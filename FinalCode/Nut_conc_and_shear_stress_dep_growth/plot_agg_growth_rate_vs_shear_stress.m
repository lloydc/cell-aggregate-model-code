function plot_agg_growth_rate_vs_shear_stress(alpha,beta_1,beta_2,beta_3,c_0,c_1,c_2,sigma_1,sigma_2)

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

d=2e-4;
mu=1e-3;
Q_in=2.205e-5;
C_in=0.22;
Sigma_scale=d^2/(mu*Q_in);
Growth_rate_scale=1/13.5; %=lengthscale(mm)/timescale(days)

c=0.19/C_in;
duudy=0:.001:0.3/Sigma_scale;

prolif_rate=g_p(c,c_1,c_2,duudy,alpha,sigma_1,sigma_2);
death_rate=g_d(c,c_0,duudy,beta_1,beta_2,beta_3,sigma_2);
growth_rate=prolif_rate-death_rate;

figure; axes('FontSize',16)
plot(Sigma_scale*duudy,Growth_rate_scale*growth_rate,'LineWidth',2)
% xlabel('Shear stress (Pa)'); xlim([0 Sigma_scale*duudy(end)]); ylabel('Aggregate growth rate (cm/day)');
xlabel('\sigma_{e,xy} (Pa)'); xlim([0 Sigma_scale*duudy(end)]); ylabel('Aggregate growth rate (cm/day)');

% surf(C_in*c,Sigma_scale*duudy,Growth_rate_scale*growth_rate,'EdgeColor','None');
% xlabel('c_u (mol/m^3)'); ylabel('\sigma_{u,xy} (Pa)'); zlabel('Aggregate growth rate (cm/day)');
% xlim([0 C_in]); ylim([0 Sigma_scale*duudy(end)]); zlim([-0.14 0.1])
% view([-61 28])

