clear all

addpath(genpath('~/Documents/MATLAB/CellAggregateModelCode/FinalCode'))))

% Set input parameters
dt=5e-3;
N_agg=5;
total_agg_length=0.25;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.01;
Sigma_2=0.05;
% Q_in=[3e-7,1e-6,2e-6,1e-5,3e-5];
% Pl_out=[15,15.5,16,17.5,19]*6895;

Q_in=[6e-7,1e-6,2e-6,5e-6,1e-5,2e-5,3e-5];
Pl_out=(15.1:0.1:15.8)*6895;

t_conf=NaN(length(Q_in),length(Pl_out));
final_tot_agg_length=NaN(length(Q_in),length(Pl_out));

% for j=1:2
%     [t_conf(1,j),final_tot_agg_length(1,j)]=nut_conc_and_shear_stress_dep_growth_nonlin_upt(dt,Q_in(1),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
%     save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_FILL_IN_temp
% end


for i=4:length(Q_in)
    for j=i+1:length(Pl_out)  %2*i+floor(i/2)
       [t_conf(i,j),final_tot_agg_length(i,j)]=nut_conc_and_shear_stress_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
    end
    save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_FILL_IN_temp
end

save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_FILL_IN
