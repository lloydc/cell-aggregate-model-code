clear all

addpath(genpath('~/Documents/MATLAB/CellAggregateModelCode/FinalCode'))

% Set input parameters
dt=5e-3;
N_agg=5;
total_agg_length=0.25;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.01;
Sigma_2=0.05;
% Q_in=[3e-7,1e-6,2e-6,1e-5,3e-5];
% Pl_out=[15,15.5,16,17.5,19]*6895;

Q_in=[3e-7,6e-7,1e-6,2e-6,5e-6,1e-5,1.5e-5,2e-5,2.5e-5,2.75e-5,3e-5];
Pl_out=(15:0.1:16)*6895;

t_conf=NaN(length(Q_in),length(Pl_out));
final_tot_agg_length=NaN(length(Q_in),length(Pl_out));

i=1; j=1;
[t_conf(i,j),final_tot_agg_length(i,j)]=nut_conc_and_shear_stress_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_temp

for i=2:3
    for j=1:i+1
        [t_conf(i,j),final_tot_agg_length(i,j)]=nut_conc_and_shear_stress_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
        save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_temp
    end
end

for i=4:length(Q_in)
    for j=1:i+1
        [t_conf(i,j),final_tot_agg_length(i,j)]=nut_conc_and_shear_stress_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
        save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT_temp
    end
end

save conf_time_vs_flow_rate_and_out_press_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT

T_scale=13.5; % time scale for aggregate growth in days
figure; axes('FontSize',16);
contourf(Pl_out,Q_in,T_scale*t_conf,20); cb=colorbar; set(cb,'FontSize',16); colormap gray
xlabel('P_{l,out} (Pa)'); ylabel('Q_{in} (m^2/s)');
title('Confluence time (days)','FontSize',16)

saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT.fig','fig')
saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_AND_SHEAR_STRESS_DEP_DET_SIM_CONT_PLOT.eps','epsc')
