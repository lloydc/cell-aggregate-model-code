function plot_conf_time_vs_flow_rate_2D_NUT_AND_SHEAR_DEP(Q_in,Pl_out,t_conf)

figure; axes('FontSize',16); 

T_scale=13.5; % time scale for aggregate growth in days

for i=1:2 %length(Pl_out)
%     % normal plotting
%     Hdls(i)=semilogx(Q_in,T_scale*t_conf(:,i),'LineWidth',2); hold all
    
    % interpolated plotting
    Qin_intp=Q_in(i):1e-7:Q_in(end);
    coeffs=polyfit(Q_in(i:end)',t_conf(i:end,i),200);
    t_conf_intp=polyval(coeffs,Qin_intp);    
    Hdls(i)=semilogx(Qin_intp,T_scale*t_conf_intp,'LineWidth',2); hold all
end

for i=3:4 %length(Pl_out)
%     % normal plotting
%     Hdls(i)=semilogx(Q_in,T_scale*t_conf(:,i),'LineWidth',2); hold all
    
    % interpolated plotting
    [Qin_intp,t_conf_intp]=smoothLine(Q_in(i:end),t_conf(i:end,i),10);    
    Hdls(i)=semilogx(Qin_intp,T_scale*t_conf_intp,'LineWidth',2); hold all
end

xlabel('Q_{in} (m^2/s)'); ylabel('t_c (days)'); legend(Hdls,['P_{l,out}=' num2str(Pl_out(1)) 'Pa'],['P_{l,out}=' num2str(Pl_out(2)) 'Pa'],['P_{l,out}' num2str(Pl_out(3)) 'Pa'],['P_{l,out}' num2str(Pl_out(4)) 'Pa'])

end

