function plot_results_surf_vs_flow_rate_vs_out_press(X,Y,Z,str)

figure; axes('FontSize',16); 
surf(X,Y,Z,'EdgeColor','None') %,'FaceColor','None');
% Plot isolated points and connect points along matrix diagonal
hold on; plot3(X(end-2:end-1),[Y(end),Y(end)],[Z(end,end-2),Z(end,end-1)],'k-')
hold on; plot3([X(1),X(1)],Y(1:2),[Z(1,1),Z(2,1)],'k-')
for i=1:length(Y)-1
   hold on; plot3([X(i),X(i+1)],[Y(i),Y(i+1)],[Z(i,i),Z(i+1,i+1)],'k-')
end    
set(gca,'yscale','log'); xlim([X(1) X(end-1)]); ylim([1e-7 1e-4]); xlabel('P_{l,out} (Pa)','FontSize',16); ylabel('Q_{in} (m^2/s)','FontSize',16); zlabel(str,'FontSize',16); view([30 22])

end

