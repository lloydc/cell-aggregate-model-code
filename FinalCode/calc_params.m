function [u,pl_out,rPe_l,gamma,c_half]=calc_params(Q_in,Pl_out,Gamma,C_half)

%%%% Dimensional parameters
d=2e-4; % lower region height
l=0.1; % membrane length
mu=1e-3; % dynamic viscosity of fluid 
P_atm=14.69*6895; % atmospheric pressure
km=2.39e-16; % membrane permeability
C_in=0.22; % inlet nutrient concentration
D_l=3e-9; % nutrient diffusivity in lower region
D_m=3e-10; % nutrient diffusivity in membrane
D_u=3e-9; % nutrient diffusivity in upper region

%%%% Convert to SI units
Q_in=Q_in*1e-6/(2*pi*d*60); % convert ml/min to m^3/s
Pl_out=Pl_out*6895; % convert psi to Pa

%%%% Dimensionless parameters
epsilon=d/l; % aspect ratio
H=1; % dimensionless height of lower region
H_m=2; % dimensionless height of top of membrane
H_u=5; % dimensionless height of top of upper region
L=1; % dimensionless length of domain

%%% Fluid transport parameters
% Transport parameters calculated from inputs
u=Q_in/d; % lower region flow velocity;
pl_out=epsilon^2*l*(Pl_out-P_atm)./(mu*u); % dimensionless lower region outlet pressure
q_in=1; % inlet flowrate
pu_out=0; % upper region outlet pressure

phi=0.77; % membrane porosity
kappa_m=epsilon^2*d^2/km;  % parameter representing dimensionless membrane permeability

% membrane fluid conductance
kf_low=0.1; % 1e16;  % low through the cell aggregate
kf_high=1; % 1e16; % high outside the cell aggregate

% Mass transport parameters
% reduced Peclet numbers
rPe_l=epsilon^2*u*l/D_l; % lower region reduced Peclet number
rPe_m=10*rPe_l; % membrane reduced Peclet number
rPe_u=rPe_l; % upper region reduced Peclet number

% Uptake parameters
gamma=d*Gamma/(D_u*C_in); % dimensionless uptake flux
c_half=C_half/C_in; % concentration at which uptake flux is half maximal

% % Growth parameters
% c_1=C_1/C_in; % minimum concentration for cell growth
% c_2=C_2/C_in; % concentration above which constant maximal aggregate growth rate is achieved
% c_0=C_0/C_in; % concentration below which cells die
% % alpha=0; % const aggregate elongate rate for cell prolif with medium shear stress
% % beta_1=0; % const aggregate shrinking rate for cell death with high shear stress
% sigma_1=epsilon*l/(mu*u)*Sigma_1; % dimensionless shear stress threshold for higher constant growth rate 
% sigma_2=epsilon*l/(mu*u)*Sigma_2; % dimensionless shear stress threshold for cell death