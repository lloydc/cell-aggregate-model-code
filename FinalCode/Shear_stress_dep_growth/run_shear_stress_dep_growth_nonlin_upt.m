clear all

% Set input parameters
dt=1e-2;
N_agg=5;
total_agg_length=0.25;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.02;
C_2=0.08;
alpha=2;
beta_1=0;
beta_2=1;
beta_3=0;
Sigma_1=0.05;
Sigma_2=0.16;
Q_in=[3e-7,1e-6,2e-6,1e-5,3e-5];
Pl_out=[15,15.5,16,17.5,19]*6895;

t_conf=NaN(length(Q_in),length(Pl_out));
final_tot_agg_length=NaN(length(Q_in),length(Pl_out));

for i=1:length(Q_in)
j=1;
    while j<=i
       [t_conf(i,j),final_tot_agg_length(i,j)]=shear_stress_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
       j=j+1;
    end
end

save conf_time_vs_flow_rate_and_outlet_pressure_SHEAR_STRESS_DEP_ONLY_nonlin_upt

T_scale=13.5; % time scale for aggregate growth in days
figure; axes('FontSize',16); 
surf(Pl_out,Q_in,T_scale*t_conf,'FaceColor','None'); 
xlabel('P_{l,out} (Pa)','FontSize',16); ylabel('Q_{in} (m^2/s)','FontSize',16); zlabel('t_c (days)','FontSize',16);

saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_SHEAR_STRESS_DEP_ONLY_nonlin_upt.fig','fig')
saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_SHEAR_STRESS_DEP_ONLY_nonlin_upt.eps','epsc')

l=0.1; % bioreactor length scale in m 
figure; axes('FontSize',16); 
surf(Pl_out,Q_in,l*final_tot_agg_length,'FaceColor','None'); 
xlabel('P_{l,out} (Pa)','FontSize',16); ylabel('Q_{in} (m^2/s)','FontSize',16); zlabel('L_{tot} (m)','FontSize',16);

saveas(gcf,'final_total_agg_length_vs_flow_rate_and_outlet_pressure_SHEAR_STRESS_DEP_ONLY_nonlin_upt.fig','fig')
saveas(gcf,'final_total_agg_length_vs_flow_rate_and_outlet_pressure_SHEAR_STRESS_DEP_ONLY_nonlin_upt.eps','epsc')