function DuDy_uHm=shear_stress_across_memb_upp_surf(Q_in,Pl_out,N_agg,total_agg_length,l_agg_std_dev,sd)

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

clc;
close all;
% clear all;

%% Define input parameters
% Number of grid spacings in x- and y-directions
Nx=400; % no. in x
Ny=80; % no. in y in lower region
Ny_m=80; % no. in y in membrane
Ny_lm=Ny+Ny_m;
Ny_u=240; % no. in y in upper region

%%%% Dimensional parameters
d=2e-4; % lower region height
l=0.1; % membrane length
mu=1e-3; % dynamic viscosity of fluid 
P_atm=14.69*6895; % atmospheric pressure
km=2.39e-16; % membrane permeability

%%%% Dimensionless parameters
epsilon=d/l; % aspect ratio
H=1; % dimensionless height of lower region
H_m=2; % dimensionless height of top of membrane
H_u=5; % dimensionless height of top of upper region
L=1; % dimensionless length of domain

%%% Fluid transport parameters
% Transport parameters calculated from inputs
u=Q_in/d; % lower region flow velocity;
pl_out=epsilon^2*l*(Pl_out-P_atm)/(mu*u); % dimensionless lower region outlet pressure
q_in=1; % inlet flowrate
pu_out=0; % upper region outlet pressure

phi=0.77; % membrane porosity
kappa_m=epsilon^2*d^2/km;  % parameter representing dimensionless membrane permeability

% membrane fluid conductance
kf_low=0.1; % 1e16;  % low through the cell aggregate
kf_high=1; % 1e16; % high outside the cell aggregate

%% Determine the node coordinate matrix P and element connectivit matrix TRI
[P_lm,~]=node_coord_and_local_node_matrices(Nx,Ny_lm,L,H_m,0);
[P_u,~]=node_coord_and_local_node_matrices(Nx,Ny_u,L,H_u-H_m,H_m);

% Calculate number of x and y points in mesh
nx=Nx+1; % number of x pts
ny=Ny_lm+Ny_u+1; % number of y pts

%%%% Calculate no. of nodes in each region
no_nodes_l=nx*(Ny+1);
no_nodes_m=nx*Ny_m;
no_nodes_lm=nx*(Ny_lm+1);
no_nodes_u=nx*(Ny_u+1);
total_no_nodes=no_nodes_lm+no_nodes_u;

%% Make vectors of x coordinates of grids for fluid and mass transport problems
% Finer grid for fluid transport problem (Make sure grid spacing is much smaller 
% than length of cell aggregate) 
Nx_int=1001;
h=1/(Nx_int-1);
x_int=0:h:1;

% Coarser grid for mass transport problem
x=0:L/Nx:L;

%% Define initial distribution of cell aggregates
X=initial_agg_distn(N_agg,total_agg_length,l_agg_std_dev,L,sd);

% Set constant for sharpness of transition in indicator function in and out
% of cell aggregates
k_trans=500;

% Solve fluid transport problem
[pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);
% [pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport_no_agg_feedback(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);

% Compute pressures and velocities
[~,~,~,~,~,~,~,~,~,~,~,~,DuDy_u] = two_dim_fluid_pressure_and_vel(P_lm,P_u,nx,Ny,Ny_m,Ny_u,no_nodes_l,no_nodes_m,pl,dpldx,d2pldx2,pu,dpudx,d2pudx2,H,H_m,H_u,kappa_m,phi);

DuDy_uHm=mu*u/d*DuDy_u(1:nx);
