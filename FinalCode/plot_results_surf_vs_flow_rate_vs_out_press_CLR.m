function plot_results_surf_vs_flow_rate_vs_out_press_CLR(X,Y,Z,str)

figure; axes('FontSize',16); 
surf(X,Y,Z,'EdgeColor','None'); shading interp    
set(gca,'yscale','log'); xlim([X(1) X(end-1)]); ylim([1e-7 1e-4]); xlabel('P_{l,out} (Pa)','FontSize',16); ylabel('Q_{in} (m^2/s)','FontSize',16); zlabel(str,'FontSize',16); view([30 22])

end

