function plot_conf_time_vs_outlet_pressure_2D_NUT_DEP(Q_in,Pl_out,t_conf)

figure; axes('FontSize',16); 

T_scale=13.5; % time scale for aggregate growth in days

Hdls=NaN(1,length(Q_in));
Hdls(1)=plot(Pl_out(1),T_scale*t_conf(1,1),'k.','MarkerSize',16); hold all

for i=2:length(Q_in)
% Hdls(i)=plot(Pl_out,T_scale*t_conf,'LineWidth',2); hold all

if i==2
    Pl_out_intp=Pl_out(1):1e3:Pl_out(i);
    coeffs=polyfit(Pl_out(1:i)',t_conf(i,1:i)',2);
    t_conf_intp=polyval(coeffs,Pl_out_intp);    
    Hdls(i)=plot(Pl_out_intp,T_scale*t_conf_intp,'LineWidth',2); hold all
else
    % interpolated plotting
    [Pl_out_intp,t_conf_intp]=smoothLine(Pl_out(1:i),t_conf(i,1:i),10);    
    Hdls(i)=plot(Pl_out_intp,T_scale*t_conf_intp,'LineWidth',2); hold all
end
    
%     Pl_out_intp=Pl_out(1):1e3:Pl_out(i)
%     coeffs=polyfit(Pl_out(1:i)',t_conf(i,1:i)',2)
%     t_conf_intp=polyval(coeffs,Pl_out_intp)    
%     Hdls(i)=plot(Pl_out_intp,T_scale*t_conf_intp,'LineWidth',2); hold all
end
xlabel('P_{l,out} (Pa)'); ylabel('t_c (days)'); legend(Hdls,['Q_{in}=' num2str(Q_in(1)) 'm^2/s'],['Q_{in}=' num2str(Q_in(2)) 'm^2/s'],['Q_{in}=' num2str(Q_in(3)) 'm^2/s'],['Q_{in}=' num2str(Q_in(4)) 'm^2/s'],['Q_{in}=' num2str(Q_in(5)) 'm^2/s'],['Q_{in}=' num2str(Q_in(6)) 'm^2/s'],['Q_{in}=' num2str(Q_in(7)) 'm^2/s'],['Q_{in}=' num2str(Q_in(8)) 'm^2/s'])

end

