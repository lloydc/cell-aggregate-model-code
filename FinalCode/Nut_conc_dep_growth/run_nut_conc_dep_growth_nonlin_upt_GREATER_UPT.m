clear all

addpath(genpath('~/Documents/MATLAB/CellAggregateModelCode/FinalCode'))

% Set input parameters - use those for rat hepatocytes
dt=1e-2;
N_agg=5;
total_agg_length=0.25;
Gamma=6.5e-6;
C_half=6.24e-3;
C_0=2.1e-2;
C_1=0.1;
C_2=0.2;
alpha=2;
beta_1=0.5;
beta_2=0;
beta_3=0;
Sigma_1=1;
Sigma_2=2;
Q_in=[3e-7,1e-6,2e-6,1e-5,3e-5];
Pl_out=[15,15.5,16,17.5,19]*6895;

t_conf=NaN(length(Q_in),length(Pl_out));
final_tot_agg_length=NaN(length(Q_in),length(Pl_out));

for i=1:length(Q_in)
    j=1;
    while j<=i
       [t_conf(i,j),final_tot_agg_length(i,j)]=nut_conc_dep_growth_nonlin_upt(dt,Q_in(i),Pl_out(j),N_agg,total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0);
       j=j+1;
    end
    save conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT_temp
end

save conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT

T_scale=13.5; % time scale for aggregate growth in days
plot_lim_param_sweep_results(Pl_out,Q_in,T_scale*t_conf,'t_c (days)')

saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT.fig','fig')
saveas(gcf,'conf_time_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT.eps','epsc')

l=0.1; % bioreactor length scale in m 
plot_lim_param_sweep_results(Pl_out,Q_in,l*final_tot_agg_length,'L_{tot} (m)')

saveas(gcf,'final_total_agg_length_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT.fig','fig')
saveas(gcf,'final_total_agg_length_vs_flow_rate_and_outlet_pressure_NUT_CONC_DEP_ONLY_nonlin_upt_GREATER_UPT.eps','epsc')