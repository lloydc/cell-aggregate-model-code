clear all

% Set input parameters
Nx=400;
dt=1e-2;
l=0.1;
N_agg=5;
total_agg_length=0.25;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.1;
C_2=0.21;
alpha=2;
beta_1=0.5;
beta_2=0;
beta_3=0;
Sigma_1=1;
Sigma_2=2;

% Q_in=3e-5;
Q_in=[3e-7,1e-6,2e-6,1e-5,3e-5];
Pl_out=[15,15.5,16,17.5,19]*6895;

%% Nutrient flux versus flow rate
nut_flux_Hm=zeros(Nx+1,length(Q_in));

for i=1:length(Q_in)
nut_flux_Hm(:,i)=nut_flux_across_memb_upp_surf(Q_in(i),Pl_out(1),N_agg,total_agg_length,[],1,Gamma,C_half);
end

save nut_flux_vs_flow_rate_nonlin_upt

x=0:1/Nx:1;
Hdls=NaN(1,length(Q_in));
figure; axes('FontSize',16)
for i=1:length(Q_in)
Hdls(i)=plot(l*x,nut_flux_Hm(:,i),'LineWidth',2); hold all
end
xlabel('x (m)'); 
% ylabel('Advective oxygen flux (mol/(m^2s)'); 
ylabel('c_ev_e|_{y=H_m}');
ylim([4e-7 1e-6])
hdl_lgd1=legend(Hdls,['Q_{in}=' num2str(Q_in(1)) 'm^2/s'],['Q_{in}=' num2str(Q_in(2)) 'm^2/s'],['Q_{in}=' num2str(Q_in(3)) 'm^2/s'],['Q_{in}=' num2str(Q_in(4)) 'm^2/s'],['Q_{in}=' num2str(Q_in(5)) 'm^2/s']);
set(hdl_lgd1,'FontSize',12)

saveas(gcf,'nut_flux_diff_flow_rates_nonlin_upt.fig','fig')
saveas(gcf,'nut_flux_diff_flow_rates_nonlin_upt.eps','epsc')

%% Nutrient flux vs outlet pressure

nut_flux_Hm2=zeros(Nx+1,length(Pl_out));

for i=1:length(Pl_out)
nut_flux_Hm2(:,i)=nut_flux_across_memb_upp_surf(Q_in(end),Pl_out(i),N_agg,total_agg_length,[],1,Gamma,C_half);
end

save nut_flux_vs_out_press_nonlin_upt

Hdls2=NaN(1,length(Pl_out));
figure; axes('FontSize',16)
for i=1:length(Pl_out)
Hdls2(i)=plot(l*x,nut_flux_Hm2(:,i),'LineWidth',2); hold all
end
xlabel('x (m)'); 
% ylabel('Advective oxygen flux (mol/(m^2s)');
ylabel('c_ev_e|_{y=H_m}');
ylim([0 1.6e-5])
hdl_lgd2=legend(Hdls2,['Pl_{out}=' num2str(Pl_out(1)) 'Pa'],['Pl_{out}=' num2str(Pl_out(2)) 'Pa'],['Pl_{out}=' num2str(Pl_out(3)) 'Pa'],['Pl_{out}=' num2str(Pl_out(4)) 'Pa'],['Pl_{out}=' num2str(Pl_out(5)) 'Pa']);
set(hdl_lgd2,'FontSize',12)

saveas(gcf,'nut_flux_diff_out_press_nonlin_upt.fig','fig')
saveas(gcf,'nut_flux_diff_out_press_nonlin_upt.eps','epsc')
