function plot_conf_time_vs_outlet_pressure_2D(Q_in,Pl_out,t_conf)

figure; axes('FontSize',16); 

T_scale=13.5; % time scale for aggregate growth in days

Hdls=NaN(1,length(Q_in));
Hdls(1)=plot(Pl_out(1),T_scale*t_conf(1,1),'ks','MarkerSize',4,'MarkerFaceColor','k'); hold all

for i=2:length(Q_in)
Hdls(i)=plot(Pl_out,T_scale*t_conf(i,:),'LineWidth',2); hold all

end
xlim([1.03e5 1.25e5]); ylim([T_scale*min(min(t_conf))-1 35]); xlabel('P_{l,out} (Pa)'); ylabel('t_c (days)'); h=legend(Hdls,['Q_{in}=' num2str(Q_in(1)) 'm^2/s'],['Q_{in}=' num2str(Q_in(2)) 'm^2/s'],['Q_{in}=' num2str(Q_in(3)) 'm^2/s'],['Q_{in}=' num2str(Q_in(4)) 'm^2/s'],['Q_{in}=' num2str(Q_in(5)) 'm^2/s'],['Q_{in}=' num2str(Q_in(6)) 'm^2/s'],['Q_{in}=' num2str(Q_in(7)) 'm^2/s'],['Q_{in}=' num2str(Q_in(8)) 'm^2/s']);
set(h,'FontSize',12)

end

