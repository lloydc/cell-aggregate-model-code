function gof=plot_best_fit_curve_and_find_gof(x,y,str1,str2,meth)

[yfit,gof] = fit(x,y,meth);
figure; axes('FontSize',16);
scatter(x,y,25,'fill'); hold on
plot(yfit,'k-')
xlabel(str1,'FontSize',16); ylabel(str2,'FontSize',16); xlim([0 0.1]); %ylim([0 30])
