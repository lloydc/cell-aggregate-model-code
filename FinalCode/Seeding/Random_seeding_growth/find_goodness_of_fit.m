function [SSresid,rsq,rmse]=find_goodness_of_fit(x,y,str1,str2)

n = length(y);

p = polyfit(x,y,6);

yfit = polyval(p,x);

figure; axes('FontSize',16);
scatter(x,y,'.'); hold on
plot(x,yfit,'k-','LineWidth',2);
xlabel(str1,'FontSize',16); ylabel(str2,'FontSize',16); xlim([0.01 0.09]); ylim([0 30])

yresid = y - yfit;

SSresid = sum(yresid.^2);

SStotal = (n-1) * var(y);

rsq = 1 - SSresid/SStotal;

rmse = sqrt(SSresid/n);