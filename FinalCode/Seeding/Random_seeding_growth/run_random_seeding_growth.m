clear all

addpath(genpath('~/Documents/MATLAB/CellAggregateModelCode/FinalCode'))

% Set input parameters
dt=1e-2;
N_agg=1:10;
total_agg_length=0.25;
mean_agg_length=total_agg_length./N_agg; % mean aggregate length
L=1; % dimensionless membrane length

l_agg_std_dev=NaN(length(N_agg),5);
for i=1:length(N_agg)
l_agg_std_dev(i,:)=0:mean_agg_length(i)/8:mean_agg_length(i)/2;
end

Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.03;
Sigma_2=0.08;
Q_in=3e-5;
Pl_out=16*6895;
frame_spac=15;

% Q_in=[3e-7,6e-7,1e-6,2e-6,5e-6,1e-5,2e-5,3e-5];
% Pl_out=[15,15.25,15.5,15.75,16,16.5,17,18,19]*6895;

t_conf=NaN(length(N_agg),size(l_agg_std_dev,2));
final_tot_agg_length=NaN(length(N_agg),size(l_agg_std_dev,2));
agg_ctr_mass=NaN(length(N_agg),size(l_agg_std_dev,2));
init_distns=NaN(length(N_agg),2,length(N_agg),size(l_agg_std_dev,2));
std_dev_agg_lengths=NaN(length(N_agg),size(l_agg_std_dev,2));

for i=1:length(N_agg)
    for j=1:size(l_agg_std_dev,2)
    [X,std_dev_agg_lengths(i,j)]=unif_rand_init_agg_distn(N_agg(i),total_agg_length,l_agg_std_dev(i,j),L);
    agg_ctr_mass(i,j)=mean((X(:,1)+X(:,2))/2);
    init_distns(1:size(X,1),1:size(X,2),i,j)=X;
    [t_conf(i,j),final_tot_agg_length(i,j)]=random_seeding_growth(dt,X,Q_in,Pl_out,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac);
    end
    save conf_time_vs_mean_agg_length_and_std_dev_temp
end

save conf_time_vs_mean_agg_length_and_std_dev

l=0.1; % membrane length in m
T_scale=13.5; % time scale for aggregate growth in days

%% Confluence time vs mean agg length and std dev
mean_agg_length_mat=repmat(mean_agg_length',1,size(l_agg_std_dev,2));
figure; axes('FontSize',16); surf(l*std_dev_agg_lengths,l*mean_agg_length_mat,T_scale*t_conf); xlabel('$s_{agg}$ (m)','Interpreter','LaTeX','FontSize',16); ylabel('$\bar{L}_{agg}$ (m)','Interpreter','LaTeX','FontSize',16); zlabel('t_c (days)','FontSize',16); %,'FaceColor','None'
saveas(gcf,'conf_time_vs_mean_agg_length_and_std_dev.fig','fig')
saveas(gcf,'conf_time_vs_mean_agg_length_and_std_dev.eps','epsc')

% figure; axes('FontSize',16); surf(l*std_dev_agg_lengths,l*mean_agg_length,l*final_tot_agg_length,'FaceColor','None'); xlabel('s_{agg} (m)','FontSize',16); ylabel('$\bar{L}_{agg}$','Interpreter','LaTeX','FontSize',16); zlabel('L_{tot} (m)','FontSize',16);
% saveas(gcf,'final_tot_agg_length_vs_mean_agg_length_and_std_dev.fig','fig')
% saveas(gcf,'final_tot_agg_length_vs_mean_agg_length_and_std_dev.eps','epsc')

%% Confluence time vs agg centre of mass
figure; axes('FontSize',16); 
for i=1:length(N_agg)
    for j=1:size(l_agg_std_dev,2)
        plot(l*agg_ctr_mass(i,j),T_scale*t_conf(i,j),'.','MarkerSize',16); hold on
    end
end
xlabel('Centre of aggregate distribution (m)','FontSize',16); ylabel('t_c (days)','FontSize',16);
saveas(gcf,'conf_time_vs_agg_ctr_mass.fig','fig')
saveas(gcf,'conf_time_vs_agg_ctr_mass.eps','epsc')

% %% Confluence time vs agg centre of mass colored mean length
% figure; axes('FontSize',16);
% marker_types = {'r*', 'g+', 'b.', 'co', 'mx','k*', 'y+', 'g.', 'ro', 'bx'};
% Hdls = NaN(1, numel(marker_types));
% for i=1:length(N_agg)
%     for j=1:size(l_agg_std_dev,2)
%         Hdls(i)=plot(l*agg_ctr_mass(i,j),T_scale*t_conf(i,j),marker_types{i},'MarkerSize',10); hold on
%     end
% end
% xlabel('Centre of aggregate distribution (m)','FontSize',16); ylabel('t_c (days)','FontSize',16);
% legend(Hdls,['L_{agg}=1';'L_{agg}=2';'L_{agg}=3';'L_{agg}=4';'L_{agg}=5';'L_{agg}=6';'L_{agg}=7';'L_{agg}=8';'L_{agg}=9';'L_{agg}=0'])
% saveas(gcf,'conf_time_vs_agg_ctr_mass_colored_mean_length.fig','fig')
% saveas(gcf,'conf_time_vs_agg_ctr_mass_colored_mean_length.eps','epsc')

% figure; axes('FontSize',16); 
% for i=1:length(N_agg)
%     for j=1:size(l_agg_std_dev,2)
%         plot(l*agg_ctr_mass(i,j),l*final_tot_agg_length(i,j),'.','MarkerSize',16);
%         hold on
%     end
% end
% xlabel('Centre of aggregate distribution (m)','FontSize',16); ylabel('L_{tot} (m)','FontSize',16);
% saveas(gcf,'final_tot_agg_length_vs_agg_ctr_mass.fig','fig')
% saveas(gcf,'final_tot_agg_length_vs_agg_ctr_mass.eps','epsc')

%% Confluence time vs mean agg length
figure; axes('FontSize',16); 
for i=1:length(N_agg)
    for j=1:size(l_agg_std_dev,2)
        plot(l*mean_agg_length(i),T_scale*t_conf(i,j),'.','MarkerSize',16); hold on
    end
end
xlabel('$\bar{L}_{agg}$ (m)','Interpreter','LaTeX','FontSize',16); ylabel('t_c (days)','FontSize',16);
saveas(gcf,'conf_time_vs_mean_agg_length.fig','fig')
saveas(gcf,'conf_time_vs_mean_agg_length.eps','epsc')

%% Confluence time vs std dev agg length
figure; axes('FontSize',16); 
for i=1:length(N_agg)
    for j=1:size(l_agg_std_dev,2)
        plot(l*std_dev_agg_lengths(i,j),T_scale*t_conf(i,j),'.','MarkerSize',16); hold on
    end
end
xlabel('$s_{agg}$ (m)','Interpreter','LaTeX','FontSize',16); ylabel('t_c (days)','FontSize',16);
saveas(gcf,'conf_time_vs_std_dev_agg_length.fig','fig')
saveas(gcf,'conf_time_vs_std_dev_agg_length.eps','epsc')

%% Confluence time vs agg ctr mass with least squares regression line
n=length(N_agg)*size(l_agg_std_dev,2);
agg_ctr_mass_rshp=reshape(agg_ctr_mass,n,1);
t_conf_rshp=reshape(t_conf,n,1);
% [ss_resid,rsq,rmse]=find_goodness_of_fit(l*agg_ctr_mass_rshp,T_scale*t_conf_rshp,'Centre of aggregate distribution (m)','t_c (days)')
gof=plot_best_fit_curve_and_find_gof(l*agg_ctr_mass_rshp,T_scale*t_conf_rshp,'Centre of aggregate distribution (m)','t_c (days)')
saveas(gcf,'conf_time_vs_agg_ctr_mass_with_best_fit.fig','fig')
saveas(gcf,'conf_time_vs_agg_ctr_mass_with_best_fit.eps','epsc')