function plot_conf_time_vs_agg_ctr_mass(str)

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

load(str)

l=0.1; % membrane length in m
T_scale=13.5; % time scale for aggregate growth in days
n=size(l_agg_std_dev,2);
agg_ctr_mass_rshp=reshape(agg_ctr_mass(1,:),n,1);
t_conf_rshp=reshape(t_conf(1,:),n,1);
% [ss_resid,rsq,rmse]=find_goodness_of_fit(l*agg_ctr_mass_rshp,T_scale*t_conf_rshp,'Centre of aggregate distribution (m)','t_c (days)')
gof=plot_best_fit_curve_and_find_gof(l*agg_ctr_mass_rshp,T_scale*t_conf_rshp,'Centre of aggregate distribution (m)','t_c (days)','poly3')
