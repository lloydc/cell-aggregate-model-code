clear all

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

% Set input parameters
dt=1e-2;
N_agg=1:10;
total_agg_length=0.25;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.03;
Sigma_2=0.08;
Q_in=3e-5;
Pl_out=[15.5,16,16.5]*6895;
frame_spac=15;

% Q_in=[3e-7,6e-7,1e-6,2e-6,5e-6,1e-5,2e-5,3e-5];
% Pl_out=[15,15.25,15.5,15.75,16,16.5,17,18,19]*6895;

t_conf=NaN(length(Pl_out),length(N_agg));
final_tot_agg_length=NaN(length(Pl_out),length(N_agg));

for i=1:length(Pl_out)
    for j=1:length(N_agg)
        [t_conf(i,j),final_tot_agg_length(i,j)]=growth_vs_seeding(dt,Q_in,Pl_out(i),N_agg(j),total_agg_length,[],1,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,0,frame_spac);
        save conf_time_vs_agg_length_nonlin_upt_temp
    end
end

save conf_time_vs_agg_length_nonlin_upt

T_scale=13.5; % time scale for aggregate growth in days
agg_length=total_agg_length./N_agg;

Hdls=NaN(1,length(Pl_out));
figure; axes('FontSize',16)
for i=1:length(Pl_out)
Hdls(i)=plot(agg_length,T_scale*t_conf(i,:),'LineWidth',2); hold all
end
xlabel('L_{agg} (m)'); ylabel('t_c (days)'); legend(Hdls,['P_{l,out}=' num2str(Pl_out(1)) 'Pa'],['P_{l,out}=' num2str(Pl_out(2)) 'Pa'],['P_{l,out}=' num2str(Pl_out(3)) 'Pa'])

saveas(gcf,'conf_time_vs_agg_length_nonlin_upt.fig','fig')
saveas(gcf,'conf_time_vs_agg_length_nonlin_upt.eps','epsc')

l=0.1; % bioreactor length scale in m 
Hdls2=NaN(1,length(Pl_out));
figure; axes('FontSize',16)
for i=1:length(Pl_out)
Hdls2(i)=plot(agg_length,l*final_tot_agg_length(i,:),'LineWidth',2); hold all
end
xlabel('L_{agg} (m)'); ylabel('L_{tot} (days)'); legend(Hdls2,['P_{l,out}=' num2str(Pl_out(1)) 'Pa'],['P_{l,out}=' num2str(Pl_out(2)) 'Pa'],['P_{l,out}=' num2str(Pl_out(3)) 'Pa'])

saveas(gcf,'final_total_agg_length_vs_agg_length_nonlin_upt.fig','fig')
saveas(gcf,'final_total_agg_length_vs_agg_length_nonlin_upt.eps','epsc')
