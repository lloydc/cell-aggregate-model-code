function [t_conf,final_total_agg_length]=init_seeding_input_growth(dt,X,Q_in,Pl_out,gl,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac,str)

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

clc;
close all;
% clear all;

%% Define input parameters
% Number of grid spacings in x- and y-directions
Nx=400; % no. in x
Ny=80; % no. in y in lower region
Ny_m=80; % no. in y in membrane
Ny_lm=Ny+Ny_m;
Ny_u=240; % no. in y in upper region

T=3;
Nt=T/dt;

%%%% Dimensional parameters
d=2e-4; % lower region height
l=0.1; % membrane length
T_scale=13.5; % time scale for aggregate growth in days
mu=1e-3; % dynamic viscosity of fluid 
P_atm=14.69*6895; % atmospheric pressure
km=2.39e-16; % membrane permeability
C_in=0.22; % inlet nutrient concentration
D_l=3e-9; % nutrient diffusivity in lower region
D_m=3e-10; % nutrient diffusivity in membrane
D_u=3e-9; % nutrient diffusivity in upper region

%%%% Dimensionless parameters
epsilon=d/l; % aspect ratio
H=1; % dimensionless height of lower region
H_m=2; % dimensionless height of top of membrane
H_u=5; % dimensionless height of top of upper region
L=1; % dimensionless length of domain

%%% Fluid transport parameters
% Transport parameters calculated from inputs
u=Q_in/d; % lower region flow velocity;
pl_out=epsilon^2*l*(Pl_out-P_atm)/(mu*u); % dimensionless lower region outlet pressure
q_in=1; % inlet flowrate
pu_out=0; % upper region outlet pressure

phi=0.77; % membrane porosity
kappa_m=epsilon^2*d^2/km;  % parameter representing dimensionless membrane permeability

% membrane fluid conductance
kf_low=0.1; % 1e16;  % low through the cell aggregate
kf_high=1; % 1e16; % high outside the cell aggregate

% Mass transport parameters
% reduced Peclet numbers
rPe_l=epsilon^2*u*l/D_l; % lower region reduced Peclet number
rPe_m=10*rPe_l; % membrane reduced Peclet number
rPe_u=rPe_l; % upper region reduced Peclet number

% Uptake parameters
gamma=d*Gamma/(D_u*C_in); % dimensionless uptake flux
c_half=C_half/C_in; % concentration at which uptake flux is half maximal

% Growth parameters
c_1=C_1/C_in; % minimum concentration for cell growth
c_2=C_2/C_in; % concentration above which constant maximal aggregate growth rate is achieved
c_0=C_0/C_in; % concentration below which cells die
% alpha=0; % const aggregate elongate rate for cell prolif with medium shear stress
% beta_1=0; % const aggregate shrinking rate for cell death with high shear stress
sigma_1=epsilon*l/(mu*u)*Sigma_1; % dimensionless shear stress threshold for higher constant growth rate 
sigma_2=epsilon*l/(mu*u)*Sigma_2; % dimensionless shear stress threshold for cell death

%% Determine the node coordinate matrix P and element connectivit matrix TRI
[P_lm,TRI_lm]=node_coord_and_local_node_matrices(Nx,Ny_lm,L,H_m,0);
[P_u,TRI_u]=node_coord_and_local_node_matrices(Nx,Ny_u,L,H_u-H_m,H_m);

% Calculate number of x and y points in mesh
nx=Nx+1; % number of x pts
ny=Ny_lm+Ny_u+1; % number of y pts

%%%% Calculate no. of nodes in each region
no_nodes_l=nx*(Ny+1);
no_nodes_m=nx*Ny_m;
no_nodes_lm=nx*(Ny_lm+1);
no_nodes_u=nx*(Ny_u+1);
total_no_nodes=no_nodes_lm+no_nodes_u;

%% Make vectors of x coordinates of grids for fluid and mass transport problems
% Finer grid for fluid transport problem (Make sure grid spacing is much smaller 
% than length of cell aggregate) 
Nx_int=1001;
h=1/(Nx_int-1);
x_int=0:h:1;

% Coarser grid for mass transport problem
x=0:L/Nx:L;

%% Define initial distribution of cell aggregates
% X=initial_agg_distn(N_agg,total_agg_length,l_agg_std_dev,L,sd);

Xprev=[0,0;X(1:end-1,:)];
Xnext=[X(2:end,:);1,1];
% Set constant for sharpness of transition in indicator function in and out
% of cell aggregates
k_trans=500;

%% Define a and b coeffs in A=diag([a,b]) anisotropy matrix
a=epsilon^2;
b=1;

%% Neumann BCs
% %%% Determine Neumann boundary edges
% NBE_lm=Neu_bnd(P_lm,TRI_lm,L,H,H_m,H_u);
% NBE_u=Neu_bnd(P_u,TRI_u,L,H,H_m,H_u);
% 
% %%% Calculate contributions from Neumann b.c.
% int_g_N_lm=Neu_bnd_cont(P_lm,NBE_lm,L,epsilon,H,H_m,H_u);
% int_g_N_u=Neu_bnd_cont(P_u,NBE_u,L,epsilon,H,H_m,H_u);

%% Dirichlet BCs
%%% Find Dirichlet boundary nodes
[~,~,~,Bnd4,~,Bnd6_m,~,~,~,~]=Dir_bnd_nodes(P_lm,L,H,H_m,H_u);
[~,~,~,~,~,Bnd6_u,~,~,~,~]=Dir_bnd_nodes(P_u,L,H,H_m,H_u);

%%% Set Dirichlet boundary as combination of parts of boundary
Dbn_lm=Bnd4;

%% Robin BCS
%%% Determine Robin boundary edges
[RBE_lm,RBelts_lm] = Rob_bnd(P_lm,TRI_lm,L,H_m);
[RBE_u,~] = Rob_bnd(P_u,TRI_u,L,H_m);

%% Plot FE mesh showing Neumann edges and Dirichlet boundary nodes
% figure; title('Mesh: red = Neumann bndry edges, green = Dirichlet boundary nodes, cyan = Robin bndry edges');
% xlabel('x');
% ylabel('y');
% hold on
% plotmesh(P_lm,TRI_lm,NBE_lm,Dbn_lm,RBE_lm); hold on
% plotmesh(P_u,TRI_u,NBE_u,[],RBE_u);

%% Calculate the Laplacian stiffness matrix K_lap
K_lap_lm=global_lap_stiff_and_mass_matrices(P_lm,TRI_lm,a,b,H,D_l,D_m);
K_lap_u=global_lap_stiff_and_mass_matrices(P_u,TRI_u,a,b,H,1,1);

%% Make RHS vector    
b_lm=zeros(no_nodes_lm,1);
b_u=zeros(no_nodes_u,1);

%%% Impose the Dirichley boundary condition by overwriting the entries
%%% of b at the Dirichlet bnd nodes
b_lm=overwrite_b([],[],[],Bnd4,[],[],[],[],[],[],P_lm,b_lm);
b_u=overwrite_b([],[],[],[],[],[],[],[],[],[],P_u,b_u);
b_lmu=[b_lm;b_u];

%% Set initial condition for concentration
% Set initial condition to iterate to steady state
C_old=zeros(total_no_nodes,1);
C_new=ones(total_no_nodes,1);

% Solve fluid transport problem
[pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);
% [pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport_no_agg_feedback(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);

% Compute pressures and velocities
[~,~,~,~,~,U_lm,U_u,~,~,V_lm,V_u,~,~] = two_dim_fluid_pressure_and_vel(P_lm,P_u,nx,Ny,Ny_m,Ny_u,no_nodes_l,no_nodes_m,pl,dpldx,d2pldx2,pu,dpudx,d2pudx2,H,H_m,H_u,kappa_m,phi);

% Calculate the advection matrix K_adv for the different regions
K_adv_lm=global_advec_matrix(P_lm,TRI_lm,rPe_m,U_lm,V_lm);
K_adv_u=global_advec_matrix(P_u,TRI_u,rPe_u,U_u,V_u);

% Calculate the total stiffness matrix
K_lm=K_lap_lm+K_adv_lm;
K_u=K_lap_u+K_adv_u;

% Calculate cell indicator function for cell aggregates on membrane
cell_ind=smooth_cell_ind(x,X,k_trans);
% cell_ind=discrete_cell_ind(x,X);
% plot(x,cell_ind)

% Specify solution tolerance for iterated initial condition
tol=1e-3;

% Iterate over first time step until the solution reaches steady state to generate initial condition
C_new=iterate_init_condn(P_lm,P_u,TRI_lm,K_lm,K_u,cell_ind,RBE_lm,RBelts_lm,RBE_u,Bnd6_m,Bnd6_u,Dbn_lm,b_lmu,L,Nx,no_nodes_l,no_nodes_m,no_nodes_u,x,C_old,C_new,D_m,D_u,gamma,c_half,tol);

% Use iterated solution as initial condition for concentration at top of membrane
CmHm_prev=C_new(nx*Ny_lm+1:no_nodes_lm);

% Set initial value of tota aggregate length
L_tot=NaN(1,Nt+1);
L_tot(1)=sum(X(:,2)-X(:,1));

% Enter for loop to step through time
for n=1:Nt
n
X
%% Solve fluid transport problem
[pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);
% [pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport_no_agg_feedback(x_int,h,x,X,k_trans,H,H_m,H_u,q_in,pl_out,pu_out,kappa_m,kf_low,kf_high);

% Compute pressures and velocities
[p_l,p_m,p_u,U_l,U_m,U_lm,U_u,V_l,V_m,V_lm,V_u,DuDy_lm,DuDy_u] = two_dim_fluid_pressure_and_vel(P_lm,P_u,nx,Ny,Ny_m,Ny_u,no_nodes_l,no_nodes_m,pl,dpldx,d2pldx2,pu,dpudx,d2pudx2,H,H_m,H_u,kappa_m,phi);

% Plot pressure and velocity surfaces
% plot_fluid_pressure_and_vel(P_lm,P_u,TRI_lm,TRI_u,p_l,p_m,p_u,U_lm,V_l,V_m,U_u,V_u,x,cell_ind,H,H_m,phi,kappa_m)
% plot_fluid_pressure_and_vel_dimensional(P_lm,P_u,TRI_lm,TRI_u,p_l,p_m,p_u,U_lm,V_l,V_m,U_u,V_u,x,cell_ind,H,H_m,phi,mu,u,epsilon,l,km,P_atm)

%% Calculate the advection matrix K_adv for the different regions
K_adv_lm=global_advec_matrix(P_lm,TRI_lm,rPe_m,U_lm,V_lm);
K_adv_u=global_advec_matrix(P_u,TRI_u,rPe_u,U_u,V_u);

%% Calculate the total stiffness matrix
K_lm=K_lap_lm+K_adv_lm;
K_u=K_lap_u+K_adv_u;

%% Calculate cell indicator function for cell aggregates on membrane
cell_ind=smooth_cell_ind(x,X,k_trans);
% cell_ind=discrete_cell_ind(x,X);
% plot(x,cell_ind)

%% Calculate Robin BC matrices
R_u=Robin_bnd_condn_matrix_nonlin_upt(P_u,RBE_u,gamma,cell_ind,CmHm_prev,c_half,L,Nx);

%%% Calculate contributions from Robin b.c.
% int_g_R_l=Rob_bnd_cont(P_l,RBE_l,0);
% int_g_R_m=Rob_bnd_cont(P_m,RBE_m,0);
% int_g_R_u=Rob_bnd_cont(P_u,RBE_u,0);

%% Assemble monolithic LHS matrix
A=[K_lm,sparse(no_nodes_lm,no_nodes_u); sparse(no_nodes_u,no_nodes_lm),K_u+R_u];

% Add contribution from integral of normal derivative on Robin boundary to
% LHS matrix
A=Rob_bnd_norm_deriv_cont(A,P_lm,TRI_lm,RBE_lm,RBelts_lm,Nx,D_m/D_u);

%%% Overwrite the rows of the monolithic matrix for the Dirichlet
%%% boundary nodes so that the Dirichlet b.c.s can be satisfied
A(Bnd6_m,:)=0;
for i=1:length(Bnd6_m)
   A(Bnd6_m(i),Bnd6_m(i))=1;
   A(Bnd6_m(i),no_nodes_lm+Bnd6_u(i))=-1;
end

%%% Overwrite the rows of the LHS matrix for the Dirichlet
%%% boundary nodes so that the Dirichlet b.c.s can be satisfied
A(Dbn_lm,:)=0;
for i=1:length(Dbn_lm)
    A(Dbn_lm(i),Dbn_lm(i))=1;
end

%% Calculate the FE approx C at time t=n*dt from the eqn AC=b_lmu
C=A\b_lmu;
C_lm=C(1:no_nodes_lm);
C_u=C(no_nodes_lm+1:total_no_nodes);

%% Grow cell aggregates
% Make colummn vectors of concentration and shear stress along top of membrane
CmHm=C_lm(nx*Ny_lm+1:no_nodes_lm);
duudyHm=-H_u*dpudx';
% figure; plot(x,duudyHm)

% figure; plot(x,CmHm), xlabel('x'); ylabel('c(y=h_m)');

% Evaluate number of cell aggregates
N_agg=size(X,1);

% Set cell agg counter to 1
j=1;

% Grow and merge cell aggs
[X,Xprev,Xnext]=aggregate_growth_with_shear_stress_dep(x_int,x,X,j,N_agg,Xprev,Xnext,h,dt,CmHm,c_1,c_2,c_0,duudyHm,alpha,beta_1,beta_2,beta_3,sigma_1,sigma_2,gl);

% Store concentration at top of membrane for evaluating nonlinear uptake term
CmHm_prev=CmHm;

L_tot(n+1)=sum(X(:,2)-X(:,1));

if rem(n,frame_spac)==0 || n==1
    t=round(T_scale*n*dt); % time rounded to the nearest day
    plot_flow_nut_distn_and_agg_growth_dimensional(P_lm,P_u,TRI_lm,TRI_u,U_l,U_m,U_u,V_l,V_m,V_u,C_lm,C_u,x,cell_ind,Nx,nx,Ny,Ny_m,Ny_u,ny,L,H,H_m,H_u,C_in)
    set(gca,'FontSize',16)
    title(['t = ' num2str(t) ' days'],'FontSize',24)
    saveas(gcf,['agg_and_nut_distn_Pl_out=' num2str(Pl_out) '_t=', num2str(n*dt) str '.fig'],'fig')
    saveas(gcf,['agg_and_nut_distn_Pl_out=' num2str(Pl_out) '_t=', num2str(n*dt) str '.eps'],'epsc')
    plot_shear_stress_and_flow_streamlines_dimensional(P_lm,P_u,TRI_lm,TRI_u,U_l,U_m,U_u,V_l,V_m,V_u,DuDy_lm,DuDy_u,x,cell_ind,Nx,nx,Ny,Ny_m,Ny_u,ny,L,H,H_m,H_u,d,mu,u)
    set(gca,'FontSize',16)
    title(['t = ' num2str(t) ' days'],'FontSize',24)
    saveas(gcf,['agg_and_shear_stress_distn_Pl_out=' num2str(Pl_out) '_t=', num2str(n*dt) str '.fig'],'fig')
    saveas(gcf,['agg_and_shear_stress_distn_Pl_out=' num2str(Pl_out) '_t=', num2str(n*dt) str '.eps'],'epsc')
end

if X(1,1)==0 && X(1,2)==1
    t_conf=n*dt;
    final_total_agg_length=1;
    plot_agg_growth_dimensional(T_scale*(0:dt:t_conf),l*L_tot(1:n+1))
    saveas(gcf,['total_agg_length_vs_time_Pl_out=' num2str(Pl_out) '.fig'],'fig');
    saveas(gcf,['total_agg_length_vs_time_Pl_out=' num2str(Pl_out) '.eps'],'epsc');
    break
elseif n==Nt
    plot_agg_growth_dimensional(T_scale*(0:dt:T),l*L_tot)
    saveas(gcf,['total_agg_length_vs_time_Pl_out=' num2str(Pl_out) '.fig'],'fig');
    saveas(gcf,['total_agg_length_vs_time_Pl_out=' num2str(Pl_out) '.eps'],'epsc');
    t_conf=NaN;
    final_total_agg_length=L_tot(Nt+1);
end

% pause(0.1)
end

end
