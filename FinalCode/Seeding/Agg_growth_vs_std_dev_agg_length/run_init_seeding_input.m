clear all

addpath('~/Documents/MATLAB/CellAggregateModelCode/Global_functions')

% Set input parameters
dt=1e-2;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.05;
Sigma_2=0.1;
Q_in=3e-5;
Pl_out=[15,19]*6895;
frame_spac=15;
gl=2; % distributed-growth growth law

% Q_in=[3e-7,6e-7,1e-6,2e-6,5e-6,1e-5,2e-5,3e-5];
% Pl_out=[15,15.25,15.5,15.75,16,16.5,17,18,19]*6895;

t_conf_left=NaN(1,2); t_conf_right=NaN(1,2); t_conf_clumped=NaN(1,2); t_conf_even=NaN(1,2);
final_tot_agg_length_left=NaN(1,2); final_tot_agg_length_right=NaN(1,2); final_tot_agg_length_clumped=NaN(1,2); final_tot_agg_length_even=NaN(1,2);

for i=1:length(Pl_out)
%% Left-skewed distn
Xleft=[0.05,0.1;0.15,0.2;0.25,0.3;0.4,0.45;0.55,0.6];
[t_conf_left(i),final_tot_agg_length_left(i)]=init_seeding_input_growth(dt,Xleft,Q_in,Pl_out(i),gl,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac,'_left');

%% Right-skewed distribution
Xright=[0.4,0.45;0.55,0.6;0.7,0.75;0.8,0.85;0.9,0.95];
[t_conf_right(i),final_tot_agg_length_right(i)]=init_seeding_input_growth(dt,Xright,Q_in,Pl_out(i),gl,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac,'_right');

%% Clumped distribution
Xclumped=[0.45,0.5;0.525,0.575;0.6,0.65;0.7,0.75;0.8,0.85];
[t_conf_clumped(i),final_tot_agg_length_clumped(i)]=init_seeding_input_growth(dt,Xclumped,Q_in,Pl_out(i),gl,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac,'_clumped');

%% Evenly spaced
Xeven=[0.075,0.125;0.2750,0.3250;0.4750,0.5250;0.6750,0.7250;0.8750,0.9250];
[t_conf_even(i),final_tot_agg_length_even(i)]=init_seeding_input_growth(dt,Xeven,Q_in,Pl_out(i),gl,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac,'_even');

save conf_time_vs_diff_seeding_distns

end

l=0.1;
T_scale=13.5;
t_conf=[t_conf_left;t_conf_right;t_conf_clumped;t_conf_even];
final_tot_agg_length=[final_tot_agg_length_left;final_tot_agg_length_right;final_tot_agg_length_clumped;final_tot_agg_length_even];

figure; axes('FontSize',14); bar(T_scale*t_conf(:,1),0.4); xlabel('Initial aggregate distribution','FontSize',16); ylabel('t_c (days)','FontSize',16); set(gca,'XTickLabel',{'Left-skewed','Right-skewed','Clumped','Even'})
saveas(gcf,['conf_time_vs_init_agg_distn_Pl_out=' num2str(Pl_out(1)) '.fig'],'fig')
saveas(gcf,['conf_time_vs_init_agg_distn_Pl_out=' num2str(Pl_out(1)) '.eps'],'epsc')

figure; axes('FontSize',12); bar(l*final_tot_agg_length(:,2),0.4); xlabel('Initial aggregate distribution','FontSize',16); ylabel('L_{tot} (m)','FontSize',16); set(gca,'XTickLabel',{'Left-skewed','Right-skewed','Clumped','Even'})
saveas(gcf,['final_tot_agg_length_vs_init_agg_distn_Pl_out=' num2str(Pl_out(2)) '.fig'],'fig')
saveas(gcf,['final_tot_agg_length_vs_init_agg_distn_Pl_out=' num2str(Pl_out(2)) '.eps'],'epsc')
