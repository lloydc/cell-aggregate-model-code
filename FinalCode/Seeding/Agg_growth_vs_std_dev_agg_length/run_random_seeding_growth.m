clear all

addpath(genpath('~/Documents/MATLAB/CellAggregateModelCode/FinalCode'))

% Set input parameters
dt=1e-2;
N_agg=1:10;
total_agg_length=0.25;
mean_agg_length=total_agg_length./N_agg; % mean aggregate length
std_dev_agg_length=0:mean_agg_length/8:mean_agg_length/2;
Gamma=3.0806e-7;
C_half=6.9e-3;
C_0=6e-3;
C_1=0.05;
C_2=0.18;
alpha=2;
beta_1=0.5;
beta_2=0.5;
beta_3=0.5;
Sigma_1=0.03;
Sigma_2=0.08;
Q_in=3e-5;
Pl_out=16*6895;
frame_spac=15;

% Q_in=[3e-7,6e-7,1e-6,2e-6,5e-6,1e-5,2e-5,3e-5];
% Pl_out=[15,15.25,15.5,15.75,16,16.5,17,18,19]*6895;

t_conf=NaN(length(N_agg),length(std_dev_agg_length));
final_tot_agg_length=NaN(length(N_agg),length(std_dev_agg_length));
agg_ctr_mass=NaN(length(N_agg),length(std_dev_agg_length));
init_distns=NaN(10,2,10,5);

for i=1:length(N_agg)
    for j=1:length(std_dev_agg_length)
    X=initial_agg_distn(N_agg(i),total_agg_length,std_dev_agg_length(j),1,2);
    agg_ctr_mass(i,j)=mean((X(:,1)+X(:,2))/2);
    init_distns(1:size(X,1),1:size(X,2),i,j)=X;
    [t_conf(i,j),final_tot_agg_length(i,j)]=random_seeding_growth(dt,X,Q_in,Pl_out,2,Gamma,C_half,C_0,C_1,C_2,Sigma_1,Sigma_2,alpha,beta_1,beta_2,beta_3,frame_spac);
    end
    save conf_time_vs_mean_agg_length_and_std_dev_temp
end

save conf_time_vs_mean_agg_length_and_std_dev

l=0.1; % membrane length in m
T_scale=13.5; % time scale for aggregate growth in days
figure; axes('FontSize',16); surf(l*std_dev_agg_length,l*mean_agg_length,T_scale*t_conf,'FaceColor','None'); xlabel('s_{agg} (m)','FontSize',16); ylabel('$\bar{L}_{agg}$','Interpreter','LaTeX','FontSize',16); zlabel('t_c (days)','FontSize',16);
saveas(gcf,'conf_time_vs_mean_agg_length_and_std_dev.fig','fig')
saveas(gcf,'conf_time_vs_mean_agg_length_and_std_dev.eps','epsc')

figure; axes('FontSize',16); surf(l*std_dev_agg_length,l*mean_agg_length,l*final_tot_agg_length,'FaceColor','None'); xlabel('s_{agg} (m)','FontSize',16); ylabel('$\bar{L}_{agg}$','Interpreter','LaTeX','FontSize',16); zlabel('L_{tot} (m)','FontSize',16);
saveas(gcf,'final_tot_agg_length_vs_mean_agg_length_and_std_dev.fig','fig')
saveas(gcf,'final_tot_agg_length_vs_mean_agg_length_and_std_dev.eps','epsc')

figure; axes('FontSize',16); 
for i=1:length(N_agg)
    for j=1:length(std_dev_agg_length)
        plot(l*agg_ctr_mass(i,j),T_scale*t_conf(i,j),'.','MarkerSize',16)
    end
end
xlabel('Centre of aggregate distribution (m)','FontSize',16); ylabel('t_c (days)','FontSize',16);
saveas(gcf,'conf_time_vs_agg_ctr_mass.fig','fig')
saveas(gcf,'conf_time_vs_agg_ctr_mass.eps','epsc')

figure; axes('FontSize',16); 
for i=1:length(N_agg)
    for j=1:length(std_dev_agg_length)
        plot(l*agg_ctr_mass(i,j),l*final_tot_agg_length(i,j),'.','MarkerSize',16)
    end
end
xlabel('Centre of aggregate distribution (m)','FontSize',16); ylabel('L_{tot} (m)','FontSize',16);
saveas(gcf,'final_tot_agg_length_vs_agg_ctr_mass.fig','fig')
saveas(gcf,'final_tot_agg_length_vs_agg_ctr_mass.eps','epsc')