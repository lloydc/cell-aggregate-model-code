function plot_final_tot_agg_length_vs_outlet_pressure_2D(Q_in,Pl_out,final_tot_agg_length)

figure; axes('FontSize',16); 

L=0.1; % fibre length

for i=1:length(Q_in)
    % normal plotting
    Hdls(i)=plot(Pl_out/6895,L*final_tot_agg_length(i,:),'LineWidth',2); hold all
    
%     % interpolated plotting
%     Qin_intp=Q_in(i):1e-7:Q_in(end);
%     coeffs=polyfit(Q_in(i:end)',final_tot_agg_length(i:end,i),200);
%     final_tot_agg_length_intp=polyval(coeffs,Qin_intp);    
%     Hdls(i)=plot(Qin_intp,L*final_tot_agg_length_intp,'LineWidth',2); hold all
end
xlabel('P_{l,out} (psi)'); ylabel('L_{tot} (m)'); legend(Hdls,['Q_{in}=' num2str(Q_in(1)) 'm^2/s'],['Q_{in}=' num2str(Q_in(2)) 'm^2/s'],['Q_{in}=' num2str(Q_in(3)) 'm^2/s'],['Q_{in}=' num2str(Q_in(4)) 'm^2/s'],['Q_{in}=' num2str(Q_in(5)) 'm^2/s'],['Q_{in}=' num2str(Q_in(6)) 'm^2/s'],['Q_{in}=' num2str(Q_in(7)) 'm^2/s'],['Q_{in}=' num2str(Q_in(8)) 'm^2/s'])

end

