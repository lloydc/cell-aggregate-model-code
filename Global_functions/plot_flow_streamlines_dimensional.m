% Plot streamlines for whole domain
function [U,V]=plot_flow_streamlines_dimensional(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,C_in,l,h_l,h_m,h_u)
yl_int=0:h_l/Ny:h_l-h_l/Ny;
ym_int=h_l:(h_m-h_l)/Ny_m:h_m;
yu_int=h_m+(h_u-h_m)/Ny_u:(h_u-h_m)/Ny_u:h_u;
y=[yl_int,ym_int,yu_int];
z=(0:1)*C_in;
[X_3D,Y_3D,Z_3D]=meshgrid(l*x,h_l*y,z);
Ul=reshape(U_l,nx,Ny+1)';
Vl=reshape(V_l,nx,Ny+1)';
Um=reshape(U_m,nx,Ny_m)';
Vm=reshape(V_m,nx,Ny_m)';
Uu=reshape(U_u(Nx+2:end),nx,Ny_u)';
Vu=reshape(V_u(Nx+2:end),nx,Ny_u)';
U=[Ul;Um;Uu];
U_3D(:,:,1)=zeros(ny,nx);
U_3D(:,:,2)=U;
V=[Vl;Vm;Vu];
V_3D(:,:,1)=zeros(ny,nx);
V_3D(:,:,2)=V;
W_3D=zeros(ny,nx,2);
[sxl,syl,szl]=meshgrid(0,0:8e-6:h_l-8e-6,C_in);
hlines=streamline(X_3D,Y_3D,Z_3D,U_3D,V_3D,W_3D,sxl,syl,szl);
set(hlines,'LineWidth',1,'Color','k')
end

