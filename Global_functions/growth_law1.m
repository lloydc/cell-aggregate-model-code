% threshold growth (linear growth above concn threshold)
function [growth_l,growth_r]=growth_law1(x,c,X,j,h,dt,c_crit,lambda)
growth_l=lambda*dt*h*sum(c>c_crit & x>X(j,1) & x<X(j,2));
growth_r=growth_l;
end

