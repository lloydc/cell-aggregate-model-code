% Plot nutrient concentration over whole domain
function plot_nut_conc_dimensional(P_lm,P_u,TRI_lm,TRI_u,C_lm,C_u,x,cell_ind,L,H,H_m,H_u,C_in)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,C_in)
xlim([0 L]); ylim([0 H_u]); xlabel('x','FontSize',16); ylabel('y','FontSize',16); title('Concentration (mol/m^3)','FontSize',16) %title(['t=' num2str(n*dt)],'FontSize',24)
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),C_in*C_lm,'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),C_in*C_u,'edgecolor','none'); hold on
shading interp
cmin=C_in*min([C_lm;C_u]);
caxis([cmin C_in]);
cb=colorbar; set(cb,'FontSize',16); %set(get(cb,'title'),'String','c','FontSize',16);
end

