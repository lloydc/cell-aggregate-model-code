%% Plot FE mesh
function plotmesh(P,TRI,NBE,Dbn,RBE)
no_elts=size(TRI,1);
no_Neu_edges=size(NBE,1);
no_Dir_nodes=size(Dbn,2);
no_Rob_edges=size(RBE,1);

% Plot elements in blue
for i=1:no_elts
    x=[P(1,TRI(i,1)),P(1,TRI(i,2)),P(1,TRI(i,3)),P(1,TRI(i,1))];
    y=[P(2,TRI(i,1)),P(2,TRI(i,2)),P(2,TRI(i,3)),P(2,TRI(i,1))];
    plot(x,y); hold on
end

% Plot Neumann bndry edges in red
for i=1:no_Neu_edges
    x=[P(1,NBE(i,1)),P(1,NBE(i,2))];
    y=[P(2,NBE(i,1)),P(2,NBE(i,2))];
    plot(x,y,'r'); hold on
end

% Plot Robin bndry edges in cyan
for i=1:no_Rob_edges
    x=[P(1,RBE(i,1)),P(1,RBE(i,2))];
    y=[P(2,RBE(i,1)),P(2,RBE(i,2))];
    plot(x,y,'c'); hold on
end

% Plot Dirichlet bndry nodes as green circles
for i=1:no_Dir_nodes
    x=P(1,Dbn(i));
    y=P(2,Dbn(i));
    plot(x,y,'go');
end

end

