function int_g_R=Rob_bnd_cont_nonlin_upt(P,RBE,Gamma,cell_ind,CmHm_prev,c_half,L,Nx)
no_Rob_edges=size(RBE,1);
int_g_R=zeros(size(P,2),1);

for i=1:no_Rob_edges
    xy1=P(:,RBE(i,1));
    xy2=P(:,RBE(i,2));
    l_edge=sqrt(sum((xy2-xy1).^2));
    g1=Rob_bnd_fn_nonlin_upt(xy1,Gamma,cell_ind,CmHm_prev,c_half,L,Nx);
    g2=Rob_bnd_fn_nonlin_upt(xy2,Gamma,cell_ind,CmHm_prev,c_half,L,Nx);
    int_g_R(RBE(i,1))=int_g_R(RBE(i,1))+l_edge*g1/2;
    int_g_R(RBE(i,2))=int_g_R(RBE(i,2))+l_edge*g2/2;
end

end
