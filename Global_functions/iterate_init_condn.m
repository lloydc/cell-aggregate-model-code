function C_new = iterate_init_condn(P_lm,P_u,TRI_lm,K_lm,K_u,cell_ind,RBE_lm,RBelts_lm,RBE_u,Bnd6_m,Bnd6_u,Dbn_lm,b_lmu,L,Nx,no_nodes_l,no_nodes_m,no_nodes_u,x,C_old,C_new,D_m,D_u,Gamma,c_half,tol)

no_nodes_lm=no_nodes_l+no_nodes_m;

%% Iterate over first time step until the solution reaches steady state to generate initial condition

figure;
while max(abs(C_new-C_old))>tol
%% Calculate Robin BC matrices
CmHm=C_new(no_nodes_lm-Nx:no_nodes_lm);
plot(x,CmHm); hold on
R_u=Robin_bnd_condn_matrix_nonlin_upt(P_u,RBE_u,Gamma,cell_ind,CmHm,c_half,L,Nx);

%% Assemble monolithic LHS matrix
A=[K_lm,sparse(no_nodes_lm,no_nodes_u); sparse(no_nodes_u,no_nodes_lm),K_u+R_u];

% Add contribution from integral of normal derivative on Robin boundary to
% LHS matrix
A=Rob_bnd_norm_deriv_cont(A,P_lm,TRI_lm,RBE_lm,RBelts_lm,Nx,D_m/D_u);

%%% Overwrite the rows of the monolithic matrix for the Dirichlet
%%% boundary nodes so that the Dirichlet b.c.s can be satisfied
A(Bnd6_m,:)=0;
for i=1:length(Bnd6_m)
   A(Bnd6_m(i),Bnd6_m(i))=1;
   A(Bnd6_m(i),no_nodes_lm+Bnd6_u(i))=-1;
end

%%% Overwrite the rows of the LHS matrix for the Dirichlet
%%% boundary nodes so that the Dirichlet b.c.s can be satisfied
A(Dbn_lm,:)=0;
for i=1:length(Dbn_lm)
    A(Dbn_lm(i),Dbn_lm(i))=1;
end

%% Calculate the FE approx C
C_old=C_new;
C_new=A\b_lmu;

end

