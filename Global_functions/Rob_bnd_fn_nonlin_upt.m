function g_R=Rob_bnd_fn_nonlin_upt(xy,Gamma,cell_ind,CmHm_prev,c_half,L,Nx)
xidx=round(xy(1)/(L/Nx))+1;
cell_indx=cell_ind(xidx);
CmHmx=CmHm_prev(xidx);
g_R=-Gamma*cell_indx*CmHmx/(c_half+CmHmx); %-Gamma;
end

