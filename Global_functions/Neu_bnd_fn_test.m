function g_N=Neu_bnd_fn_test(xy,v,L,epsilon,H,H_m,H_u)
if xy(1)==L && v(1)==L % both nodes of the edge are on x=L
    g_N=epsilon^2*xy(2);
elseif xy(2)==H_u && v(2)==H_u % both nodes are on y=H_u
    g_N=xy(1);
% elseif (xy(1)==0 && xy(2)>=H && xy(2)<=H_m && v(1)==0 && v(2)>=H && v(2)<=H_m) % both nodes are on x=0, H<y<H_m
%     g_N=-epsilon^2*xy(2);
elseif (xy(1)==0 && xy(2)>=H && v(1)==0 && v(2)>=H) % both nodes are on x=0, H<y<H_u
    g_N=-epsilon^2*xy(2);
end
end

