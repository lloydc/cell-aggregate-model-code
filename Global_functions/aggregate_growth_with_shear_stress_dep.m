%% Grow cell regions
function [X,Xprev,Xnext]=aggregate_growth_with_shear_stress_dep(x_int,x,X,j,N_agg,Xprev,Xnext,h,dt,CmHm,c_1,c_2,c_0,duudyHm,alpha,beta_1,beta_2,beta_3,sigma_1,sigma_2,gl)

% Linearly interpolate membrane-upper-region interface concentration and upper region pressure gradient 
% onto finer grid given by x_int
c=interp1q(x',CmHm,x_int');
duudy=interp1q(x',duudyHm,x_int');
% figure; plot(x,CmHm,'b-',x_int,c,'r-');
% idx_l=(abs(x_int-X(1,1))<=h/2);
% idx_r=(abs(x_int-X(1,2))<=h/2);
% sidx_l=logical([zeros(1,5),idx_l(1:end-5)]);
% sidx_r=logical([idx_r(6:end),zeros(1,5)]);
% plot(x_int,c,'b-',X(1,1),c(idx_l),'r.',X(1,2),c(idx_r),'r.'); xlabel('x'); ylabel('c_m^H^m');


    while j<=N_agg
        
        if gl==1;
            [growth_l,growth_r]=point_growth_with_shear_stress_dep(x_int,X,j,h,dt,c,c_1,c_2,c_0,duudy,alpha,beta_1,beta_2,beta_3,sigma_1,sigma_2);
        elseif gl==2
            [growth_l,growth_r]=dist_growth_with_shear_stress_dep(x_int,X,j,h,dt,c,c_1,c_2,c_0,duudy,alpha,beta_1,beta_2,beta_3,sigma_1,sigma_2);
        end
        
        if (j==1 && X(1,1)==0) % i.e. if there is at least 1 cell aggregate
            if (X(1,2)>0)
            % Check growth is smaller than gap to cell region on the right
            if growth_r<Xnext(j,1)-X(j,2)
                X(j,2)=X(j,2)+growth_r; % all growth at right hand end
            else %if growth>Xnext(j,1)-X(j,2)
                X(j,2)=Xnext(j,1);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            else % do nothing if there are no aggregates
            end
            
                
        elseif (j==N_agg && X(N_agg,2)==1)
            
            % Check growth is smaller than gap to cell region on the
            % left
            if growth_l<X(j,1)-Xprev(j,2)
                X(j,1)=X(j,1)-growth_l; % all growth on the left
            else %if growth>X(j,1)-Xprev(j,2)
                X(j,1)=Xprev(j,2);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            
        else
            
            % Check growth is smaller than gap to next cell
            if (growth_l<X(j,1)-Xprev(j,2) && growth_r<Xnext(j,1)-X(j,2))
                X(j,1)=X(j,1)-growth_l;
                X(j,2)=X(j,2)+growth_r;
            elseif (growth_l<X(j,1)-Xprev(j,2) && growth_r>=Xnext(j,1)-X(j,2))
                X(j,1)=X(j,1)-growth_l;
                X(j,2)=Xnext(j,1);
            elseif (growth_l>=X(j,1)-Xprev(j,2) && growth_r<Xnext(j,1)-X(j,2))
                X(j,1)=Xprev(j,2);
                X(j,2)=X(j,2)+growth_r;
            else %if (growth>X(j,1)-Xprev(j,2) && growth>Xnext(j,1)-X(j,2))
                X(j,1)=Xprev(j,2);
                X(j,2)=Xnext(j,1);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            
        end
        
        % increment cell region counter
        j=j+1;
    end
    
    % Reset counter
    j=1;
    while j<=N_agg
        % Set indicator for number of merged regions
        ind=0;
        % If cell region touches next cell region at RH end,
        % merge cell regions into one region that grows equally at either end
        if (X(j,2)>=Xnext(j,1) && X(j,1)>=Xprev(j,2) && j~=N_agg) % only merge cell regions if not considering cell region on the right
            if N_agg==2
                X=[X(1,1), X(2,2)];
                
                % Update number of cell regions
                N_agg=N_agg-1;
                Xprev=[0,0;X(1:end-1,:)];
                Xnext=[X(2:end,:);1,1];
                
            else
                if (X(j,1)>Xprev(j,2) || j==1)
                    X=[X(1:j-1,:); X(j,1), X(j+1,2); X(j+2:end,:)];
                    % Update number of cell regions
                    N_agg=N_agg-1;
                elseif (j~=1 && X(j,1)==Xprev(j,2))
                    % Change indicator to show three regions have been
                    % merged
                    ind=1;
                    X=[X(1:j-2,:); X(j-1,1), X(j+1,2); X(j+2:end,:)];
                    N_agg=N_agg-2;
                end
                
                Xprev=[0,0;X(1:end-1,:)];
                Xnext=[X(2:end,:);1,1];
            end
        end
        
        if ind==0 % if two cell regions have been merged increment counter by 1
        j=j+1;
        end
    end
end