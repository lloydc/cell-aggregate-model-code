%%%% Apply b.c.s on different parts of the Dirichlet boundary at time t
function b = overwrite_b_test(Bnd1,Bnd2,Bnd3,Bnd4,Bnd5,Bnd6,Bnd7,Bnd8,Bnd9,Bnd10,P,b)

b(Bnd1)=0;

b(Bnd4)=0;

b(Bnd7)=0;

b(Bnd10)=0;
end

