%%% Calculate contribution of Neumann b.c.s to RHS of linear system for FE approx.
function  int_g_N=Neu_bnd_cont(P,Neu_bnd_edges,L,epsilon,H,H_m,H_u)
no_Neu_edges=size(Neu_bnd_edges,1);
int_g_N=zeros(size(P,2),1);

for i=1:no_Neu_edges
    xy1=P(:,Neu_bnd_edges(i,1));
    xy2=P(:,Neu_bnd_edges(i,2));
    l_edge=sqrt(sum((xy2-xy1).^2));
    g1=Neu_bnd_fn(xy1,xy2,L,epsilon,H,H_m,H_u);
    g2=Neu_bnd_fn(xy2,xy1,L,epsilon,H,H_m,H_u);
    int_g_N(Neu_bnd_edges(i,1))=int_g_N(Neu_bnd_edges(i,1))+l_edge*g1/2;
    int_g_N(Neu_bnd_edges(i,2))=int_g_N(Neu_bnd_edges(i,2))+l_edge*g2/2;
end
end

