function plot_cell_ind(x,cell_ind)
figure; axes('FontSize',14); 
plot(x,cell_ind,'LineWidth',2); ylim([-0.01 1.01]); xlabel('x','FontSize',16); ylabel('I_c(x,t)','FontSize',16);
end

