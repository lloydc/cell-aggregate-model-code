%% Make (2 x number of nodes) node coordinate matrix P and (number of elements x 3) local node matrix TRI manually
function [P,TRI]=node_coord_and_local_node_matrices(Nx,Ny,Lx,Ly,y0)
no_nodes=(Nx+1)*(Ny+1);
x=zeros(1,no_nodes);
y=zeros(1,no_nodes);

% Define node numbers and node coordinates
for j=1:Ny+1
    for i=1:Nx+1
        node=i+(j-1)*(Nx+1);
        x(node)=(i-1)/Nx*Lx;
        y(node)=y0+(j-1)/Ny*Ly;
    end
end

% Make node coord matrix
P=[x;y];

no_elts=2*Nx*Ny;
TRI=zeros(no_elts,3);

% Define local node matrix
for j=1:Ny
    for i=1:Nx
        elt=2*((i-1)+(j-1)*Nx)+1;
        TRI(elt,1:3)=[i+(j-1)*(Nx+1),i+1+(j-1)*(Nx+1),i+j*(Nx+1)];
        elt=elt+1;
        TRI(elt,1:3)=[i+1+j*(Nx+1),i+j*(Nx+1),i+1+(j-1)*(Nx+1)];
    end
end

end
