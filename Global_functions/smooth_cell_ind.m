% Smoothed cell indicator function
function cell_ind=smooth_cell_ind(x,X,k)

Ncell=size(X,1);
Nx=length(x);
cell_ind=zeros(1,Nx);

for i=1:Ncell
    cell_ind=cell_ind+1/2*(tanh(k*(x-X(i,1)))+tanh(k*(X(i,2)-x)));
end

end

