%%% Determine the Neumann boundary edges and return them in the matrix NBE
function NBE = Neu_bnd(P,TRI,L,H,H_m,H_u)

no_elts=size(TRI,1);
NBE=[];

for i=1:no_elts
    %%% Find the nodes of element i
    elt_nodes=TRI(i,:);
    %%% Put the node coordinates in a matrix xy
    xy=P(:,elt_nodes);
    %%% Make a matrix with the edges of the element
    edges=[elt_nodes([1,2]);elt_nodes([2,3]);elt_nodes([3,1])];
    %%% Calculate the edge centres
    edge_centres=[(xy(:,1)+xy(:,2))/2, (xy(:,2)+xy(:,3))/2, (xy(:,3)+xy(:,1))/2];
    
    %%% For each edge centre check if it is on the Neumann boundary and
    %%% store the edge in NBE if it is
    for j=1:3
        x=edge_centres(1,j);
        y=edge_centres(2,j);
        if  y==0 || (x==L && y<H) || (x==L && y>H && y<H_m) || (x==0 && y>H && y<H_m)  || (x==L && y>H_m) || y==H_u || (x==0 && y>H_m)
            NBE=[NBE;edges(j,:)];
        end
    end
    
end
end

