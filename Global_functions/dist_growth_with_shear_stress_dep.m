% Distributed growth (integral of excess conc over aggregate length)
function [growth_l,growth_r]=dist_growth_with_shear_stress_dep(x,X,j,h,dt,c,c_1,c_2,c_0,duudy,alpha,beta_1,beta_2,beta_3,sigma_1,sigma_2)

if  rem(X(j,1),h)~=0 || rem(X(j,2),h)~=0 % one aggregate end is not on a grid pt
    c_l=interpolate_conc(x,c,X(j,1),h);
    c_r=interpolate_conc(x,c,X(j,2),h);
    duudy_l=interpolate_conc(x,duudy,X(j,1),h);
    duudy_r=interpolate_conc(x,duudy,X(j,2),h);
    idx_l=ceil(X(j,1)/h)+1;
    idx_r=floor(X(j,2)/h)+1;
    f=g_p(c,c_1,c_2,duudy,alpha,sigma_1,sigma_2)-g_d(c,c_0,duudy,beta_1,beta_2,beta_3,sigma_2);
    f_l=g_p(c_l,c_1,c_2,duudy_l,alpha,sigma_1,sigma_2)-g_d(c_l,c_0,duudy_l,beta_1,beta_2,beta_3,sigma_2);
    f_r=g_p(c_r,c_1,c_2,duudy_r,alpha,sigma_1,sigma_2)-g_d(c_r,c_0,duudy_r,beta_1,beta_2,beta_3,sigma_2);
    growth_l=tr_iep(x,X,j,dt,h,f,idx_l,idx_r,f_l,f_r); % trapezium rule with half-width trapezium at interpolated ends of aggregate
else
    idx_l=round(X(j,1)/h)+1;
    c_l=c(idx_l);
    duudy_l=duudy(idx_l);
    idx_r=round(X(j,2)/h)+1;
    c_r=c(idx_r);
    duudy_r=duudy(idx_r);
    f=g_p(c,c_1,c_2,duudy,alpha,sigma_1,sigma_2)-g_d(c,c_0,duudy,beta_1,beta_2,beta_3,sigma_2);
    f_l=g_p(c_l,c_1,c_2,duudy_l,alpha,sigma_1,sigma_2)-g_d(c_l,c_0,duudy_l,beta_1,beta_2,beta_3,sigma_2);
    f_r=g_p(c_r,c_1,c_2,duudy_r,alpha,sigma_1,sigma_2)-g_d(c_r,c_0,duudy_r,beta_1,beta_2,beta_3,sigma_2);
    growth_l=tr(x,X,j,dt,h,f,f_l,f_r); % trapezium rule for concentration integral
end

growth_r=growth_l;
end

function trap_rule=tr(x,X,j,dt,h,f,f_l,f_r)
trap_rule=dt*h*((x>X(j,1) & x<X(j,2))*f+1/2*(f_l+f_r));
end

function trap_rule_with_interpolated_end_pts=tr_iep(x,X,j,dt,h,f,idx_l,idx_r,f_l,f_r)
trap_rule_with_interpolated_end_pts=dt*(h*((x>X(j,1) & x<X(j,2))*f-1/2*(f(idx_l)+f(idx_r)))+(x(idx_l)-X(j,1))/2*(f(idx_l)+f_l)+(X(j,2)-x(idx_r))/2*(f(idx_r)+f_r));
end