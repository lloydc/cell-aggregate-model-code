%% Grow cell regions
function [X,Xprev,Xnext]=cell_region_growth(x_int,x,X,j,Ncell,Xprev,Xnext,h,dt,CmHm,c_crit,lambda,gl)

% Linearly interpolate membrane-upper-region interface concentration onto
% finer grid given by x_int
c=interp1q(x',CmHm,x_int');
% figure; plot(x,CmHm,'b-',x_int,c,'r-');
% idx_l=(abs(x_int-X(1,1))<=h/2);
% idx_r=(abs(x_int-X(1,2))<=h/2);
% sidx_l=logical([zeros(1,5),idx_l(1:end-5)]);
% sidx_r=logical([idx_r(6:end),zeros(1,5)]);
% plot(x_int,c,'b-',X(1,1),c(idx_l),'r.',X(1,2),c(idx_r),'r.'); xlabel('x'); ylabel('c_m^H^m');


    while j<=Ncell
        
        if gl==1;
            [growth_l,growth_r]=point_growth(x_int,c,X,j,h,dt,c_crit,lambda);
        elseif gl==2
            [growth_l,growth_r]=dist_growth(x_int,c,X,j,h,dt,c_crit,lambda);
        end
        
        if (j==1 && X(1,1)==0) % i.e. if there is at least 1 cell aggregate
            if (X(1,2)>0)
            % Check growth is smaller than gap to cell region on the right
            if growth_r<Xnext(j,1)-X(j,2)
                X(j,2)=X(j,2)+growth_r; % all growth at right hand end
            else %if growth>Xnext(j,1)-X(j,2)
                X(j,2)=Xnext(j,1);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            else % do nothing if there are no aggregates
            end
            
                
        elseif (j==Ncell && X(Ncell,2)==1)
            
            % Check growth is smaller than gap to cell region on the
            % left
            if growth_l<X(j,1)-Xprev(j,2)
                X(j,1)=X(j,1)-growth_l; % all growth on the left
            else %if growth>X(j,1)-Xprev(j,2)
                X(j,1)=Xprev(j,2);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            
        else
            
            % Check growth is smaller than gap to next cell
            if (growth_l<X(j,1)-Xprev(j,2) && growth_r<Xnext(j,1)-X(j,2))
                X(j,1)=X(j,1)-growth_l;
                X(j,2)=X(j,2)+growth_r;
            elseif (growth_l<X(j,1)-Xprev(j,2) && growth_r>=Xnext(j,1)-X(j,2))
                X(j,1)=X(j,1)-growth_l;
                X(j,2)=Xnext(j,1);
            elseif (growth_l>=X(j,1)-Xprev(j,2) && growth_r<Xnext(j,1)-X(j,2))
                X(j,1)=Xprev(j,2);
                X(j,2)=X(j,2)+growth_r;
            else %if (growth>X(j,1)-Xprev(j,2) && growth>Xnext(j,1)-X(j,2))
                X(j,1)=Xprev(j,2);
                X(j,2)=Xnext(j,1);
            end
            Xprev=[0,0;X(1:end-1,:)];
            Xnext=[X(2:end,:);1,1];
            
        end
        
        % increment cell region counter
        j=j+1;
    end
    
    % Reset counter
    j=1;
    while j<=Ncell
        % Set indicator for number of merged regions
        ind=0;
        % If cell region touches next cell region at RH end,
        % merge cell regions into one region that grows equally at either end
        if (X(j,2)>=Xnext(j,1) && X(j,1)>=Xprev(j,2) && j~=Ncell) % only merge cell regions if not considering cell region on the right
            if Ncell==2
                X=[X(1,1), X(2,2)];
                
                % Update number of cell regions
                Ncell=Ncell-1;
                Xprev=[0,0;X(1:end-1,:)];
                Xnext=[X(2:end,:);1,1];
                
            else
                if (X(j,1)>Xprev(j,2) || j==1)
                    X=[X(1:j-1,:); X(j,1), X(j+1,2); X(j+2:end,:)];
                    % Update number of cell regions
                    Ncell=Ncell-1;
                elseif (j~=1 && X(j,1)==Xprev(j,2))
                    % Change indicator to show three regions have been
                    % merged
                    ind=1;
                    X=[X(1:j-2,:); X(j-1,1), X(j+1,2); X(j+2:end,:)];
                    Ncell=Ncell-2;
                end
                
                Xprev=[0,0;X(1:end-1,:)];
                Xnext=[X(2:end,:);1,1];
            end
        end
        
        if ind==0 % if two cell regions have been merged increment counter by 1
        j=j+1;
        end
    end
end