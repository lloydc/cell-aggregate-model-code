% Distributed growth (integral of excess conc over aggregate length)
function [growth_l,growth_r]=dist_growth(x,c,X,j,h,dt,c_crit,lambda)

if rem(X(j,1),h)==0 && rem(X(j,2),h)==0 % both aggregates are on grid pts N.B. There will be a problem if both end pts of the aggregate are not on grid pts - I don't think this will happen
    idx_l=round(X(j,1)/h)+1;
    c_l=c(idx_l);
    idx_r=round(X(j,2)/h)+1;
    c_r=c(idx_r);
    growth_l=lambda*dt*h*((x>X(j,1) & x<X(j,2))*subplus(c-c_crit)+subplus(1/2*(c_l+c_r)-c_crit)); % trapezium rule for concentration integral
else
    c_l=interpolate_conc(x,c,X(j,1),h);
    c_r=interpolate_conc(x,c,X(j,2),h);
    idx_l=ceil(X(j,1)/h)+1;
    idx_r=floor(X(j,2)/h)+1;
    growth_l=lambda*dt*(h*((x>X(j,1) & x<X(j,2))*subplus(c-c_crit)-1/2*(subplus(c(idx_l)-c_crit)+subplus(c(idx_r)-c_crit)))+(x(idx_l)-X(j,1))/2*(subplus(c(idx_l)-c_crit)+subplus(c_l-c_crit))+(X(j,2)-x(idx_r))/2*(subplus(c(idx_r)-c_crit)+subplus(c_r-c_crit))); % use half-width trapezium at interpolated ends of aggregate
end

growth_r=growth_l;
end

