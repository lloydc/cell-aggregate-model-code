% Plot streamlines with arrows for whole domain using streamslice
function [U,V]=plot_flow_streamlines_with_streamslice(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,H,H_m,H_u)
yl_int=0:H/Ny:H-H/Ny;
ym_int=H:(H_m-H)/Ny_m:H_m;
yu_int=H_m+(H_u-H_m)/Ny_u:(H_u-H_m)/Ny_u:H_u;
y=[yl_int,ym_int,yu_int];
z=0:1;
[X_3D,Y_3D,Z_3D]=meshgrid(x,y,z);
Ul=reshape(U_l,nx,Ny+1)';
Vl=reshape(V_l,nx,Ny+1)';
Um=reshape(U_m,nx,Ny_m)';
Vm=reshape(V_m,nx,Ny_m)';
Uu=reshape(U_u(Nx+2:end),nx,Ny_u)';
Vu=reshape(V_u(Nx+2:end),nx,Ny_u)';
U=[Ul;Um;Uu];
U_3D(:,:,1)=zeros(ny,nx);
U_3D(:,:,2)=U;
V=[Vl;Vm;Vu];
V_3D(:,:,1)=zeros(ny,nx);
V_3D(:,:,2)=V;
W_3D=zeros(ny,nx,2);
[sxl,syl,szl]=meshgrid(0,0:0.04:H-0.04,1);
hlines=streamslice(X_3D,Y_3D,Z_3D,U_3D,V_3D,W_3D,[],[],1,0.5);
set(hlines,'LineWidth',1,'Color','k')
end

