function [X,std_dev_agg_lengths]=unif_rand_init_agg_distn(N_agg,total_agg_length,l_agg_std_dev,L)
% Calculate average aggregate length
    l_agg_av=total_agg_length/N_agg;
    Q=null(ones(1,N_agg));
    agg_lengths_less_mean=l_agg_std_dev/sqrt(N_agg/(N_agg-1))*Q*randn(N_agg-1,1);
    std_dev_agg_lengths=std(agg_lengths_less_mean);
    agg_lengths=l_agg_av+agg_lengths_less_mean;
    agg_left_ends=zeros(N_agg,1);
    agg_right_ends=ones(N_agg,1);
    next_agg_left_ends=zeros(N_agg,1);
    while (sum(agg_right_ends<next_agg_left_ends)~=N_agg || agg_left_ends(1)<0)
    agg_ctrs=sort(rand(N_agg,1));
    agg_left_ends=agg_ctrs-agg_lengths/2;
    agg_right_ends=agg_ctrs+agg_lengths/2;
    next_agg_left_ends=[agg_left_ends(2:end);L];
    X=[agg_left_ends,agg_right_ends];
    end
end

