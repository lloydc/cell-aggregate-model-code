% Nutrient concentration and shear stress dependent aggregate growth term
function nut_conc_and_shear_stress_dep_prolif=g_p(c,c_1,c_2,duudy,alpha,sigma_1,sigma_2)
nut_conc_and_shear_stress_dep_prolif=(heaviside(c-c_1).*heaviside(c_2-c).*(c-c_1) + heaviside(c-c_2).*(c_2-c_1)).*(heaviside(sigma_1-duudy) + alpha*heaviside(duudy-sigma_1).*heaviside(sigma_2-duudy));
end