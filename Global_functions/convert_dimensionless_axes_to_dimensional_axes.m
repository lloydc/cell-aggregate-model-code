function convert_dimensionless_axes_to_dimensional_axes()
% Label axes and set viewing angle
xlabel('x (m)','FontSize',16);
ylabel('y (10^{-4} m)','FontSize',16);
% zlabel('c','FontSize',16,'Rotation',0);
set(gca,'XTick',[0:0.2:1])
set(gca,'XTickLabel',[0;0.02;0.04;0.06;0.08;0.1])
set(gca,'YTick',[0:1:5])
set(gca,'YTickLabel',[0;2;4;6;8;10])
end