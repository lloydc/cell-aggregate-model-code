% Define DE for pressure
function dPdx=pressureODE(x,P,h,k_f,pl_out,Q_in,pu_out,H,H_m,H_u,kappa_m)
xidx=round(x/h)+1;
k=k_f(xidx);
pl=P(1); dpldx=P(2);
pu=(4*(pl_out-pl)+12*Q_in*(1-x))/(H_u-H_m)^3+pu_out;
a=1+k*kappa_m*(H_m-H);
 
dPdx=[dpldx;3*k*(pl-pu)/a];
end

