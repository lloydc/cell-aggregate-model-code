function plot_cell_ind_diff_trans_fact(x,X,alpha_vec)

figure; axes('FontSize',16);

cell_ind=zeros(length(alpha_vec),length(x));

for i=1:length(alpha_vec)
cell_ind(i,:)=smooth_cell_ind(x,X,alpha_vec(i));
end
plot(x,cell_ind(1,:),'g-',x,cell_ind(2,:),'c-',x,cell_ind(3,:),'b-','Linewidth',2); ylim([-0.01 1.01]); xlabel('x'); ylabel('i_c(x,t)'); legend(['a=' num2str(alpha_vec(1))],['a=' num2str(alpha_vec(2))],['a=' num2str(alpha_vec(3))])
end

