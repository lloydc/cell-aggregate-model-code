function q=Rob_bnd_condn_coeff_fn(xyc,Gamma,cell_ind,L,Nx)
xidx=round(xyc(1)/(L/Nx))+1;
cell_indx=cell_ind(xidx);
q=Gamma*cell_indx;
end

