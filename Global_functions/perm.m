% Calculate membrane upper surface permeability function from aggregate
% permeability, kf_low, and aggregate-free membrane permeability, kf_high
function kf=perm(cell_ind,kf_low,kf_high)
kf=kf_high*(1-cell_ind)+kf_low*cell_ind;
end

