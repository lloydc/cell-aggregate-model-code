function [K_elt,M_elt]=element_stiff_and_mass_matrices(P,TRI,elt,a,b,rPe,H,H_m,U,V,D_l,D_m)

    % Find global node numbers of input element
    nodes = TRI(elt,:);
    
    % Find x- and y- coordinates for each node of element
    x0 = P(1,nodes(1)); x1 = P(1,nodes(2)); x2 = P(1,nodes(3));
    y0 = P(2,nodes(1)); y1 = P(2,nodes(2)); y2 = P(2,nodes(3));
    
    % Calculate determinant of Jacobian of transformation of element triangle to
    % reference triangle
    det_Jac = (x1-x0)*(y2-y0)-(x2-x0)*(y1-y0);
    
    % Calculate element mass matrix
    M_elt=det_Jac*[1/12 1/24 1/24; 1/24 1/12 1/24; 1/24 1/24 1/12];
    
    % Calculate gradient of basis functions
    grad=[y1-y2,y2-y0,y0-y1;x2-x1,x0-x2,x1-x0]/det_Jac;
    
    % Define anistropy tensor
    if y0<=H
        A=D_l/D_m*diag([a,b]);
    else
        A=diag([a,b]);
    end
    
    % Find U and V values at nodes
    u=U(nodes);
    v=V(nodes);
    
    % Calculate contribution from advection with velocity (u,v) from
    % solution of fluid problem defined over mesh
    advec_cont=rPe*M_elt*[u,v]*grad;
    
    % Calculate element stiffness matrix
    K_elt = 1/2*det_Jac*grad'*A*grad+advec_cont;

end

