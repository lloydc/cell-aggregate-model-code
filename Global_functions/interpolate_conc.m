% Concentration interpolation
function c_int=interpolate_conc(x,c,Xe,h)
    idx1=floor(Xe/h)+1;
    idx2=min(idx1+1,length(x));
    x1=x(idx1);
    x2=x(idx2);
    c1=c(idx1);
    c2=c(idx2);
    c_int=c1+(c2-c1)/(x2-x1)*(Xe-x1);
end

