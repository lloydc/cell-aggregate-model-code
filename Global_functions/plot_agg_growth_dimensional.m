function plot_agg_growth_dimensional(t,L)
figure; axes('FontSize',16)
plot(t,L,'LineWidth',2)
xlabel('t (days)'); ylabel('L_{tot}(t) (m)');
end

