% Growth at a point (differential growth)
function [growth_l,growth_r]=point_growth_with_shear_stress_dep(x,X,j,h,dt,c,c_1,c_2,c_0,duudy,alpha,beta_1,sigma_1,sigma_2)
c_l=interpolate_conc(x,c,X(j,1),h);
c_r=interpolate_conc(x,c,X(j,2),h);

duudy_l=interpolate_conc(x,duudy,X(j,1),h);
duudy_r=interpolate_conc(x,duudy,X(j,2),h);

growth_l=lambda*dt*(g_p(c_l,c_1,c_2,duudy_l,alpha,sigma_1,sigma_2)-g_d(c,c_0,duudy_l,beta_1,beta_2,beta_3,sigma_2));
growth_r=lambda*dt*(g_p(c_r,c_1,c_2,duudy_r,alpha,sigma_1,sigma_2)-g_d(c,c_0,duudy_r,beta_1,beta_2,beta_3,sigma_2));
end

