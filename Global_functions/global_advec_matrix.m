function K_adv=global_advec_matrix(P,TRI,rPe,U,V)
% Find number of elements and number of nodes
no_elts = size(TRI,1);

% Initialise row vectors to take local node numbers and contributions to the stiffness and mass matrices
I = zeros(1,9*no_elts); 
J = zeros(1,9*no_elts);
KV_adv = zeros(1,9*no_elts);
% MV = zeros(1,9*no_elts);

% Define vectors of local node numbers to enable us to take all different pairwise combinations of basis functions %%%%
nodesI = [1 2 3 1 2 3 1 2 3];
nodesJ = [1 1 1 2 2 2 3 3 3];

% For each element find the global nodes which correspond to the local
% nodes, put the global node numbers into two vectors for all pairwise
% combinations, compute the local stiffness and mass matrices, flatten them
% into row vectors and put them respectively into the vectors KV and MV 
% (consectutively for each element)
for elt=1:no_elts
    nodes=TRI(elt,:);
    I((elt-1)*9+1:elt*9)  = nodes(nodesI);
    J((elt-1)*9+1:elt*9) = nodes(nodesJ);
    K_elt_adv=element_adv_matrix(P,TRI,elt,rPe,U,V);
    KV_adv((elt-1)*9+1:elt*9) = K_elt_adv(:)';
end

% Form the global stiffness and mass matrices as sparse matrices. The
% sparse function deals with contributions to the same matrix entry from 
% different (adjacent) elements by adding together any elements of KV 
% that have duplicate values of I and J 
K_adv = sparse(I,J,KV_adv);
end
