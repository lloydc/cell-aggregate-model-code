% Make (2 x number of nodes) node coordinate matrix P and (number of elements x 3) 
% element connectivity matrix TRI using inbuilt delaunay triangulation method
function [P,TRI] = node_coords_elt_conn_matrix_delaunay(Nx,Ny,Lx,Ly,y0)

% Make vectors of Nx+1 equally spaced x points and Ny+1 equally spaced y
% points
x = 0:Lx/Nx:Lx;
y = y0:Ly/Ny:y0+Ly;

% Form a grid from the vectors of x and y points
[X,Y] = meshgrid(x,y);

% Flatten matrices of x and y coordinates and put them into P
x = X(:); y = Y(:);
P = [x'; y'];

% Generate TRI using in-built delaunay triangulation function
TRI = delaunay(x,y);
no_elts = size(TRI,1);

% Loop through the elements to check for negative Jacobians
for elt = 1:no_elts
    
    % Find nodes and coordinates for given element
    nodes = TRI(elt,:);
    pts = P(:,nodes);
    
    % Calculate the determinant of the Jacobian for the transformation to
    % the reference triangle
    det_Jac = (pts(1,2)-pts(1,1))*(pts(2,3)-pts(2,1))-(pts(1,3)-pts(1,1))*(pts(2,2)-pts(2,1));
    % Check to see if the determinant is negative. If it is, swap the 2nd 
    % and 3rd element node numbers in TRI.
    if det_Jac < 0
        TRI(elt,2) = nodes(3);
        TRI(elt,3) = nodes(2);
    end
end
end


