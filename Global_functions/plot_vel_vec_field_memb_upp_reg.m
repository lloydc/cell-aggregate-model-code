% Plot velocity vector field just in membrane and upper region
function plot_vel_vec_field_memb_upp_reg(P_lm,P_u,Um,Uu,Vm,Vu,nx,Ny,Ny_m,Ny_u)
X_2D=reshape([P_lm(1,nx*Ny+1:nx*(Ny+Ny_m+1)),P_u(1,nx+1:end)],nx,Ny_m+Ny_u+1)';
Y_2D=reshape([P_lm(2,nx*Ny+1:nx*(Ny+Ny_m+1)),P_u(2,nx+1:end)],nx,Ny_m+Ny_u+1)';
Z_2D=ones(Ny_m+Ny_u+1,nx);
U_2D=[Um;Uu];
V_2D=[Vm;Vu];
W=zeros(Ny_m+Ny_u+1,nx);
no_arws=5;
no_x=2:floor((size(X_2D,1)-2)/no_arws):size(X_2D,1)-1;
no_y=2:floor((size(X_2D,2)-2)/no_arws):size(X_2D,2)-1;
quiver3(X_2D(no_x,no_y),Y_2D(no_x,no_y),Z_2D(no_x,no_y),U_2D(no_x,no_y),V_2D(no_x,no_y),W(no_x,no_y),'Linewidth',1,'Color','k');
end