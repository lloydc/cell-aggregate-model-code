% Initial guess for lower region pressure and pressure gradient
function P0 = Pinit(x,Q_in,pl_out)
P0 = [3*Q_in*(1-x)+pl_out; -3*Q_in];
end

