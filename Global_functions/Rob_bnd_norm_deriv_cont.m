% Add contribution from integral of normal derivative on Robin boundary to
% LHS matrix
function A=Rob_bnd_norm_deriv_cont(A,P,TRI,RBE,RBelts,Nx,D_ratio)

no_Rob_edges=size(RBE,1);

for i=1:no_Rob_edges
    elt=RBelts(i);
    
    % Calculate gradient of basis functions of element
    grad=element_gradient(P,TRI,elt);

    % Find coordinates of Robin bnd edge nodes
    x0=P(1,RBE(i,1)); y0=P(2,RBE(i,1));
    x1=P(1,RBE(i,2)); y1=P(2,RBE(i,2));
    
    % Calculate length of Robin bnd edge
    l_edge=((x1-x0)^2+(y1-y0)^2)^(1/2);
       
    % Calculate unit normal vector to Robin bnd
    norm_vec=[y0-y1;x0-x1]/l_edge;
    
    % Calculate normal derivative on Robin bnd
    norm_deriv=norm_vec'*grad;
    
    % Calculate integral contribution from normal derivative
    norm_deriv_int=D_ratio*norm_deriv*l_edge/2;
    
    % Add integral contribution to LHS matrix in row corresponding to
    % equation for Robin bnd edge node in region above and columns corresponding 
    % to nodes of element in region below with same Robin bnd edge     
    A(Nx+1+RBE(i,1),TRI(elt,:))=A(Nx+1+RBE(i,1),TRI(elt,:))+norm_deriv_int;
    A(Nx+1+RBE(i,2),TRI(elt,:))=A(Nx+1+RBE(i,2),TRI(elt,:))+norm_deriv_int;
end

end

