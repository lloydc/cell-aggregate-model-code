function R=Robin_bnd_condn_matrix_nonlin_upt(P,RBE,Gamma,cell_ind,CmHm_prev,c_half,L,Nx)
% Find number of elements and number of nodes
no_nodes = size(P,2);
no_Rob_edges = size(RBE,1);

% Initialise global stiffness and mass matrices
R = sparse(no_nodes,no_nodes);

for i=1:no_Rob_edges
% Find coordinates of Robin bnd nodes
xy1=P(:,RBE(i,1));
xy2=P(:,RBE(i,2));
% Calculate element boundary matrix
R_elt=Rob_bnd_condn_coeff_fn_nonlin_upt(xy1,xy2,Gamma,cell_ind,CmHm_prev,c_half,L,Nx);
% Put element boundary matrix in global boundary matrix
R(RBE(i,:),RBE(i,:))=R(RBE(i,:),RBE(i,:))+R_elt;
end

end