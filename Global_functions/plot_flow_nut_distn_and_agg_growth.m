%%% Plot flow, nutrient distribution and aggregate growth on same figure
function plot_flow_nut_distn_and_agg_growth(P_lm,P_u,TRI_lm,TRI_u,U_l,U_m,U_u,V_l,V_m,V_u,C_lm,C_u,x,cell_ind,Nx,nx,Ny,Ny_m,Ny_u,ny,L,H,H_m,H_u)

% Plot nutrient concentration
plot_nut_conc(P_lm,P_u,TRI_lm,TRI_u,C_lm,C_u,x,cell_ind,L,H,H_m,H_u); hold on

% Plot flow streamlines fow whole domain
[U,V,Um,Vm,Uu,Vu]=plot_flow_streamlines(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,H,H_m,H_u); hold on

% Plot velocity vector field for whole domain
plot_vel_vec_field(P_lm,P_u,U,V,Nx,nx,ny); hold off

% Plot velocity vector field just in membrane and upper region
% plot_vel_vec_field_memb_upp_reg(P_lm,P_u,Um,Uu,Vm,Vu,nx,Ny,Ny_m,Ny_u);

%%% Alternatively plot flow streamlines with direction arrows using streamslice
% [U,V]=plot_flow_streamlines_with_streamslice(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,H,H_m,H_u);

% Label axes and set viewing angle
xlabel('x','FontSize',16);
ylabel('y','FontSize',16);
zlabel('c','FontSize',16,'Rotation',0);
view([0 90])

end

