%%% Plot flow, nutrient distribution and aggregate growth on same figure
function plot_shear_stress_and_flow_streamlines_dimensional(P_lm,P_u,TRI_lm,TRI_u,U_l,U_m,U_u,V_l,V_m,V_u,DuDy_lm,DuDy_u,x,cell_ind,Nx,nx,Ny,Ny_m,Ny_u,ny,L,H,H_m,H_u,d,mu,u)

% Plot nutrient concentration
plot_shear_stress_dimensional(P_lm,P_u,TRI_lm,TRI_u,DuDy_lm,DuDy_u,x,cell_ind,L,H,H_m,H_u,d,mu,u); hold on

% Plot flow streamlines fow whole domain
[U,V,Um,Vm,Uu,Vu]=plot_flow_streamlines(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,H,H_m,H_u); hold on

% Plot velocity vector field for whole domain
plot_vel_vec_field(P_lm,P_u,U,V,Nx,nx,ny); hold off

% Plot velocity vector field just in membrane and upper region
% plot_vel_vec_field_memb_upp_reg(P_lm,P_u,Um,Uu,Vm,Vu,nx,Ny,Ny_m,Ny_u);

%%% Alternatively plot flow streamlines with direction arrows using streamslice
% plot_flow_streamlines_with_streamslice(U_l,U_m,U_u,V_l,V_m,V_u,x,Nx,nx,Ny,Ny_m,Ny_u,ny,H,H_m,H_u);

% Label axes and set viewing angle
convert_dimensionless_axes_to_dimensional_axes()
view([0 90])

end

