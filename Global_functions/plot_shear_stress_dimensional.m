% Plot dimensional shear stress (du/dy) over whole domain
function plot_shear_stress_dimensional(P_lm,P_u,TRI_lm,TRI_u,DuDy_lm,DuDy_u,x,cell_ind,L,H,H_m,H_u,d,mu,u)
DuDymax=max(DuDy_u);
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,DuDymax)
xlim([0 L]); ylim([0 H_u]); xlabel('x','FontSize',24); ylabel('y','FontSize',24); title('Shear stress (Pa)','FontSize',24) %title(['t=' num2str(n*dt)],'FontSize',24)
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),mu*u/d*DuDy_lm,'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),mu*u/d*DuDy_u,'edgecolor','none'); hold on
shading interp
DuDymin=min([DuDy_lm;DuDy_u]);
caxis([0 mu*u/d*DuDymax]);
cb=colorbar; set(cb,'FontSize',16); %set(get(cb,'title'),'String','du/dy','FontSize',24);
end

