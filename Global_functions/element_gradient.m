% Calculate gradient of basis functions of a given element
function grad = element_gradient(P,TRI,elt)

    % Find global node numbers of input element
    nodes = TRI(elt,:);
    
    % Find x- and y- coordinates for each node of element
    x0 = P(1,nodes(1)); x1 = P(1,nodes(2)); x2 = P(1,nodes(3));
    y0 = P(2,nodes(1)); y1 = P(2,nodes(2)); y2 = P(2,nodes(3));
    
    % Calculate determinant of Jacobian of transformation of element triangle to
    % reference triangle
    det_Jac = (x1-x0)*(y2-y0)-(x2-x0)*(y1-y0);
    
    % Calculate gradient of basis functions
    grad=[y1-y2,y2-y0,y0-y1;x2-x1,x0-x2,x1-x0]/det_Jac;


end

