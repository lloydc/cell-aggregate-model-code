%% Solve fluid transport problem
function [pl,dpldx,d2pldx2,pu,dpudx,d2pudx2]=fluid_transport_no_agg_feedback(x_int,h,x,X,k_trans,H,H_m,H_u,Q_in,pl_out,pu_out,kappa_m,kf_low,kf_high)

% Define discrete cell indicator
% cell_ind=discrete_cell_ind(x,X);
% Define smooth cell indicator
cell_ind=smooth_cell_ind(x_int,X,k_trans);  
    
%% Solve pressure BVP
k_f=perm(cell_ind,kf_low,kf_high);

solinit_p = bvpinit(x_int,@(x_int)Pinit(x_int,Q_in,pl_out));
sol_p = bvp4c(@(x_int,P)pressureODE_no_agg_feedback(x_int,P,pl_out,Q_in,pu_out,H,H_m,H_u,kappa_m),@(Pa,Pb)pressureBCs(Pa,Pb,Q_in,pl_out),solinit_p);

[pl_int, dpldx_int] = deval(sol_p,x);
pl=pl_int(1,:);
dpldx=dpldx_int(1,:);
d2pldx2=dpldx_int(2,:);
pu=(4*(pl_out-pl)+12*Q_in*(1-x))/(H_u-H_m)^3+pu_out;
dpudx=-(12*Q_in+4*dpldx)/(H_u-H_m)^3;
d2pudx2=-4*d2pldx2/(H_u-H_m)^3;
% figure; plot(x_int,cell_ind)
end 

