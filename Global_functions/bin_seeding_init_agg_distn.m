function [X,total_agg_length_act]=bin_seeding_init_agg_distn(total_agg_length,N_bins,p)
    % p = probability of choosing bin on left half of membrane
    % 1-p = probability of choosing bin on right half of membrane
        bins=(0:1/N_bins:1)';
        N_aggs=round(total_agg_length*N_bins);
        total_agg_length_act=N_aggs/N_bins;
        
        idx=zeros(N_aggs,1);
        bin_probs=[p/(N_bins/2)*ones(1,N_bins/2) (1-p)/(N_bins/2)*ones(1,N_bins/2)];
        
        i=1;
        while i<=N_aggs
            r=rand;
            idx_temp=sum(r>=cumsum([0,bin_probs]));
            if sum(idx==idx_temp)==0
                idx(i)=idx_temp;
                i=i+1;
            end
        end
        
        idx=sort(idx);
        X=[bins(idx) bins(idx+1)];

        i=1;
        while i<=N_aggs-1
            if X(i,2)==X(i+1,1)
                temp=X(i+1,2);
                X=[X(1:i,:);X(i+2:end,:)];
                X(i,2)=temp;
                N_aggs=size(X,1);
            end
            i=i+1;
        end 
    
end
