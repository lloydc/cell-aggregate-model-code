% Shear-stress dependent aggregate growth term
function shear_stress_dep_growth=g_s(duudy,beta_p,beta_d,sigma_p,sigma_d)
shear_stress_dep_growth=beta_p*heaviside(duudy-sigma_p)-(beta_p+beta_d)*heaviside(duudy-sigma_d);
end