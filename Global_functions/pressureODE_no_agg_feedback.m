% Define DE for pressure
function dPdx=pressureODE_no_agg_feedback(x,P,pl_out,Q_in,pu_out,H,H_m,H_u,kappa_m)
pl=P(1); dpldx=P(2);
pu=(4*(pl_out-pl)+12*Q_in*(1-x))/(H_u-H_m)^3+pu_out;
a=kappa_m*(H_m-H);
 
dPdx=[dpldx;3*(pl-pu)/a];
end

