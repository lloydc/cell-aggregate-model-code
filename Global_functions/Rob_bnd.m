%%% Determine the Robin boundary edges and return them in the matrix NBE
function [RBE,RBelts] = Rob_bnd(P,TRI,L,y0)

no_elts=size(TRI,1);
RBE=[];
RBelts=[];

for i=1:no_elts
    %%% Find the nodes of element i
    elt_nodes=TRI(i,:);
    %%% Put the node coordinates in a matrix xy
    xy=P(:,elt_nodes);
    %%% Make a matrix with the edges of the element
    edges=[elt_nodes([1,2]);elt_nodes([2,3]);elt_nodes([3,1])];
    %%% Calculate the edge centres
    edge_centres=[(xy(:,1)+xy(:,2))/2, (xy(:,2)+xy(:,3))/2, (xy(:,3)+xy(:,1))/2];
    
    %%% For each edge centre check if it is on the Robin boundary and
    %%% store the edge in RBE if it is
    for j=1:3
        if  edge_centres(2,j)==y0
            RBE=[RBE;edges(j,:)];
            RBelts=[RBelts;i];
        end
    end
    
end
end

