function R_elt=Rob_bnd_condn_coeff_fn_nonlin_upt(xy1,xy2,Gamma,cell_ind,CmHm_prev,c_half,L,Nx)

% Calculate edge length
l_edge=(sum((xy2-xy1).^2))^(1/2);

% Find indices of end nodes on edge
xidx1=round(xy1(1)/(L/Nx))+1;
xidx2=round(xy2(1)/(L/Nx))+1;

% Compute quadrature pts
xq1=(-(xy2(1)-xy1(1))/3^(1/2)+xy1(1)+xy2(1))/2;
xq2=((xy2(1)-xy1(1))/3^(1/2)+xy1(1)+xy2(1))/2;

% Find values of concentration at the nodes from the previous time step
C1=CmHm_prev(xidx1);
C2=CmHm_prev(xidx2);

% Calculate element Robin bdry matrix by Gauss-Legende quadrature
R_elt=Gamma*l_edge/2*(intgd(xq1,xy1,xy2,xidx1,xidx2,cell_ind,C1,C2,c_half)+intgd(xq2,xy1,xy2,xidx1,xidx2,cell_ind,C1,C2,c_half));
end

% Function to evaluate integrand at quadrature points
function r=intgd(x,xy1,xy2,xidx1,xidx2,cell_ind,C1,C2,c_half)
cell_ind_intp=cell_ind(xidx1)+(cell_ind(xidx2)-cell_ind(xidx1))/(xy2(1)-xy1(1))*(x-xy1(1));
r=cell_ind_intp*[(1-x)^2 x*(1-x); x*(1-x) x^2]/(c_half+C1*(1-x)+C2*x);
end