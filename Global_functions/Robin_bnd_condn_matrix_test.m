function R=Robin_bnd_condn_matrix_test(P,RBE,Gamma,cell_ind,L,Nx)
% Find number of elements and number of nodes
no_nodes = size(P,2);
no_Rob_edges = size(RBE,1);

% Initialise global stiffness and mass matrices
R = sparse(no_nodes,no_nodes);

for i=1:no_Rob_edges
% Find coordinates of Robin bnd nodes
xy1=P(:,RBE(i,1));
xy2=P(:,RBE(i,2));
l_edge=sqrt(sum((xy2-xy1).^2));
% Calculate edge centre
xyc=(xy1+xy2)/2;
q=Rob_bnd_condn_coeff_fn_test(xyc,Gamma,cell_ind,L,Nx);
% Calculate element boundary matrix
R_elt=q*[2,1;1,2]*l_edge/6;
% Put element boundary matrix in global boundary matrix
R(RBE(i,:),RBE(i,:))=R(RBE(i,:),RBE(i,:))+R_elt;
end

end

