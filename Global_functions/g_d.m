% Shear-stress dependent aggregate growth term
function nut_conc_and_shear_stress_dep_death=g_d(c,c_0,duudy,beta_1,beta_2,beta_3,sigma_2)
nut_conc_and_shear_stress_dep_death=beta_1*heaviside(c_0-c)+beta_2*heaviside(duudy-sigma_2)+beta_3*heaviside(c_0-c).*heaviside(duudy-sigma_2);
end