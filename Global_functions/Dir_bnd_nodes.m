%%% Find nodes of each boundary for implementing Dirichlet boundary
%%% conditions
function [Bnd1,Bnd2,Bnd3,Bnd4,Bnd5,Bnd6,Bnd7,Bnd8,Bnd9,Bnd10] = Dir_bnd_nodes(P,L,H,H_m,H_u)
%%% Find nodes on y=0, 0<x<1
Bnd1=find(P(2,:)==0);

%%% Find nodes on x=L, 0<y<1
Bnd2=find(P(1,:)==L & P(2,:)<=1);

%%% Find nodes on y=H, 0<x<1
Bnd3=find(P(2,:)==H);

%%% Find nodes on x=0, 0<y<H
Bnd4=find(P(1,:)==0 & P(2,:)<=H);

%%% Find nodes on x=L, H<y<H_m
Bnd5=find(P(1,:)==L & P(2,:)>=H & P(2,:)<=H_m);

%%% Find nodes on y=H_m, 0<x<1
Bnd6=find(P(2,:)==H_m);

%%% Find nodes on x=0, H<y<H_m
Bnd7=find(P(1,:)==0 & P(2,:)>=H & P(2,:)<=H_m);

%%% Find nodes on x=1, H_m<y<H_u
Bnd8=find(P(1,:)==L & P(2,:)>=H_m);

%%% Find nodes on y=H_u, 0<x<1
Bnd9=find(P(2,:)==H_u);

%%% Find nodes on x=0, H_m<y<H_u
Bnd10=find(P(1,:)==0 & P(2,:)>=H_m);

end

