function [X,total_agg_length_act]=bin_seeding_init_agg_distn_old(total_agg_length,N_bins,wght)
    % Even seeding
    if wght==1
        bins=0:1/N_bins:1;
        N_aggs=round(total_agg_length*N_bins);
        total_agg_length_act=N_aggs/N_bins;
        
        X_temp=NaN(N_aggs,2);
        total_agg_length_temp=0;
        idx=NaN(N_aggs,1);
        
        for i=1:N_aggs
        r=(1-total_agg_length_temp)*rand(1);
        idx_temp=ceil(r*N_bins);
        num_sml_idcs=sum(idx<=idx_temp);
        if sum(idx==idx_temp+num_sml_idcs)==0
        idx(i)=idx_temp+num_sml_idcs;
        else idx(i)=idx_temp+num_sml_idcs+1;
        end
        X_temp(i,:)=[bins(idx(i)) bins(idx(i)+1)];
        total_agg_length_temp=total_agg_length_temp+1/N_bins;
        end

        X=sort(X_temp);
        
        i=1;
        while i<=N_aggs-1
            if X(i,2)==X(i+1,1)
                temp=X(i+1,2);
                X=[X(1:i,:);X(i+2:end,:)];
                X(i,2)=temp;
                N_aggs=size(X,1);
            end
            i=i+1;
        end
        
    end
    
    % Inlet-skewed seeding
    if wght~=1
        a=(wght-1)/(wght^N_bins-1);
        
        bins=NaN(N_bins+1,1);
        bins(1)=0;
        for i=2:N_bins+1
        bins(i)=bins(i-1)+a*wght^(i-2);
        end
%         figure; plot(bins,ones(length(bins),1),'bx')
        
        total_agg_length_temp=0;
        X_temp=[];
        bins_slctd=[];
        
%         break_flag=false;
        
        while total_agg_length_temp<total_agg_length
            r=(1-total_agg_length_temp)*rand(1);
            for j=1:N_bins
                if r>bins(j) && r<bins(j+1) && sum(bins_slctd==j)==0 % && abs(total_agg_length-(total_agg_length_temp+(bins(j+1)-bins(j))))<abs(total_agg_length-total_agg_length_temp)
                    X_temp=[X_temp; bins(j) bins(j+1)];
                    bins_slctd=[bins_slctd; j];
                    total_agg_length_temp=total_agg_length_temp+(bins(j+1)-bins(j));
%                 elseif abs(total_agg_length-(total_agg_length_temp+(bins(j+1)-bins(j))))>abs(total_agg_length-total_agg_length_temp)
% %                     bins(j+1)-bins(j)
%                     break_flag=true;
%                     break
                end
            end
            
%             if break_flag
%                 break
%             end
        end

        total_agg_length_act=total_agg_length_temp;
        X=sort(X_temp);
        N_aggs=size(X,1);

        i=1;
        while i<=N_aggs-1
            if X(i,2)==X(i+1,1)
                temp=X(i+1,2);
                X=[X(1:i,:);X(i+2:end,:)];
                X(i,2)=temp;
                N_aggs=size(X,1);
            end
            i=i+1;
        end

    end
    
    
end

    
    
    
    % Outlet skewed seeding
            
    




