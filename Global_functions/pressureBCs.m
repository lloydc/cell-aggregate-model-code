% Prescribe inlet flux and outlet pressure 
function res_p = pressureBCs(Pa,Pb,Q_in,pl_out)
res_p = [Pb(1)-pl_out; Pa(2)+3*Q_in];
end