% Growth at a point (differential growth)
function [growth_l,growth_r]=point_growth(x,c,X,j,h,dt,c_crit,lambda)
c_l=interpolate_conc(x,c,X(j,1),h);
c_r=interpolate_conc(x,c,X(j,2),h);

growth_l=lambda*dt*subplus(c_l-c_crit);
growth_r=lambda*dt*subplus(c_r-c_crit);
end

