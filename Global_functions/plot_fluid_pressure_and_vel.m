function plot_fluid_pressure_and_vel(P_lm,P_u,TRI_lm,TRI_u,p_l,p_m,p_u,U_lm,V_l,V_m,U_u,V_u,x,cell_ind,H,H_m,phi,kappa_m)
figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max([p_l;phi*kappa_m*p_m;p_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),[p_l;phi*kappa_m*p_m],'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),p_u,'edgecolor','none'); xlabel('x'); ylabel('y'); zlabel('p'); title('Pressure','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90]);

figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max([U_lm;U_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),U_lm,'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),U_u,'edgecolor','none'); zlim([-0.01 max(U_lm)]); xlabel('x'); ylabel('y'); zlabel('u'); title('Velocity x-component','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90])

figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max([V_l;phi*V_m;V_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),[V_l;phi*V_m],'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),V_u,'edgecolor','none'); xlabel('x'); ylabel('y'); zlabel('v'); title('Velocity y-component','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90])

end

