%%% Compute fluid velocities over mesh
function [p_l,p_m,p_u,U_l,U_m,U_lm,U_u,V_l,V_m,V_lm,V_u] = twoD_fluid_pressure_and_vel(P_lm,P_u,nx,Ny,Ny_m,Ny_u,no_nodes_l,no_nodes_m,pl,dpldx,d2pldx2,pu,dpudx,d2pudx2,H,H_m,H_u,kappa_m,phi)

DplDx=repmat(dpldx',Ny+1,1);
D2plDx2=repmat(d2pldx2',Ny+1,1);
D2plDx2_m=repmat(d2pldx2',Ny_m,1);
DpuDx=repmat(dpudx',Ny_u+1,1);
D2puDx2=repmat(d2pudx2',Ny_u+1,1);
yl=P_lm(2,1:no_nodes_l)';
ym=P_lm(2,no_nodes_l+1:end)';
yu=P_u(2,:)';

% Pressures
p_l=repmat(pl',Ny+1,1);
p_m=(p_l(1:no_nodes_m)/kappa_m-D2plDx2_m.*(ym-H)/3)/phi;
p_u=repmat(pu',Ny_u+1,1);

% Velocities
U_l=DplDx.*(yl.^2-H)/2;
U_m=zeros(no_nodes_m,1);
U_lm=[U_l;U_m];
U_u=DpuDx.*(yu-H_m).*(yu-H_u)/2;
V_l=D2plDx2.*yl.*(3-yl.^2)/6;
V_m=D2plDx2_m/(3*phi);
V_lm=[V_l;V_m];
V_u=-D2puDx2.*(yu-H_u).^2.*(2*yu-3*H_m+H_u)/12;

% Flux through the top of the membrane
VmHm=phi*V_lm(nx*(Ny+Ny_m)+1:nx*(Ny+Ny_m+1));
% plot(x,VmHm); xlabel('x'); ylabel('v(y=h_m)'); title(['Q_i_n=' num2str(q_in) 'm^2 s^-^1, P_l_,_o_u_t=' num2str(Pl_out) 'Pa']);

end

