% Plot velocity vector field for whole domain
function plot_vel_vec_field(P_lm,P_u,U,V,Nx,nx,ny)
X_2D=reshape([P_lm(1,:),P_u(1,Nx+2:end)],nx,ny)';
Y_2D=reshape([P_lm(2,:),P_u(2,Nx+2:end)],nx,ny)';
Z_2D=ones(ny,nx);
W=zeros(ny,nx);
no_arws=5;
no_x=2:floor((size(X_2D,1)-2)/no_arws):size(X_2D,1)-1;
no_y=2:floor((size(X_2D,2)-2)/no_arws):size(X_2D,2)-1;
quiver3(X_2D(no_x,no_y),Y_2D(no_x,no_y),Z_2D(no_x,no_y),U(no_x,no_y),V(no_x,no_y),W(no_x,no_y),'Linewidth',1,'Color','k');
end

