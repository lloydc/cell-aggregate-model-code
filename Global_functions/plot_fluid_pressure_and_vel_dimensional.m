function plot_fluid_pressure_and_vel_dimensional(P_lm,P_u,TRI_lm,TRI_u,p_l,p_m,p_u,U_lm,V_l,V_m,U_u,V_u,x,cell_ind,H,H_m,phi,mu,u,epsilon,l,km,P_atm)
P_l=mu*u/(epsilon^2*l);
P_m=epsilon^2*mu*u*l/km;
figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max(P_atm+[P_l*p_l;phi*P_m*p_m;P_l*p_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),[P_atm+P_l*p_l;P_atm+phi*P_m*p_m],'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),P_atm+P_l*p_u,'edgecolor','none'); 
convert_dimensionless_axes_to_dimensional_axes()
%zlabel('p'); 
title('Pressure (Pa)','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90]);

figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max(u*[U_lm;U_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),u*U_lm,'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),u*U_u,'edgecolor','none'); zlim([-0.01 max(u*U_lm)]); 
convert_dimensionless_axes_to_dimensional_axes()
%zlabel('u'); 
title('Velocity x-component (m/s)','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90])

figure; axes('FontSize',16)
plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,max(epsilon*u*[V_l;phi*V_m;V_u]));
trisurf(TRI_lm,P_lm(1,:),P_lm(2,:),[epsilon*u*V_l;phi*epsilon*u*V_m],'edgecolor','none'); hold on
trisurf(TRI_u,P_u(1,:),P_u(2,:),epsilon*u*V_u,'edgecolor','none');
convert_dimensionless_axes_to_dimensional_axes()
%zlabel('v'); 
title('Velocity y-component (m/s)','FontSize',16); cb=colorbar; set(cb,'FontSize',16); view([0 90])

end

