% Concentration dependent aggregate growth term
function conc_dep_growth=g_c(c,c_crit,lambda)
conc_dep_growth=lambda*subplus(c-c_crit);
end
