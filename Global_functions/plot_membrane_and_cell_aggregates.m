function plot_membrane_and_cell_aggregates(x,cell_ind,H,H_m,zmax)
% Set threshold for cell indicator function above which cells are "present"
cell_ind_thr=(cell_ind>0.5);
memb=H*ones(1,length(x));
z_vec=zmax*ones(1,length(x));

plot3(x(cell_ind_thr),(H_m+0.05)*memb(cell_ind_thr),z_vec(cell_ind_thr),'w.','MarkerSize',16); hold on
% plot3(x,zeros(1,length(x)),z_vec,'k--','Linewidth',2); hold on
plot3(x,memb,z_vec,'k--','Linewidth',2); hold on
plot3(x,H_m*memb,z_vec,'k--','Linewidth',2); hold on
%%% Solid line rather than dashed line for membrane surfaces
% plot3(x,H_u*memb,z_vec,'k-','Linewidth',2);
end

