% Discrete cell indicator function
function cell_ind=discrete_cell_ind(x,X)
    cell_ind=zeros(1,length(x));
    Ncell=size(X,1);
    for j=1:Ncell
        cell_ind_j=(x>=X(j,1) & x<=X(j,2));
%         cell_j=(x>X(j,1) & x<X(j,2));
        cell_ind=cell_ind+cell_ind_j;
    end
end

