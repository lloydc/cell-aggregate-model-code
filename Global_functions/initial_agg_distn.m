function X=initial_agg_distn(N_agg,total_agg_length,l_agg_std_dev,L,sd)
% Calculate average aggregate length
l_agg_av=total_agg_length/N_agg;
if sd==1
    %% Evenly spread cells
    agg_ctrs=(1/2:N_agg-1/2)'/N_agg;
    X=[agg_ctrs-l_agg_av/2,agg_ctrs+l_agg_av/2];
elseif sd==2
    %% Uniformly randomly distributed aggregates with uniformly randomly distributed lengths
%     % Make vector of left hand ends of cell regions
%     Xl=sort(rand(N_agg,1));
%     % Calculate the gaps between the starts of adjacent cell regions
%     gaps=[Xl(2:end);1]-Xl;
%     % Make vector of right hand ends of cell regions ensuring that no cell
%     % regions overlap
%     Xr=Xl+rand(N_agg,1).*gaps;
%     X=[Xl,Xr];
    %% Uniform random seeding with normally distributed aggregate lengths
%     % Make a vector of aggregate lengths normally distributed about average
%     agg_lengths=subplus(l_agg_av+randn(N_agg,1)*l_agg_std_dev);
%     temp=rand(N_agg+1,1);
%     agg_spaces=temp/sum(temp)*(L-sum(agg_lengths));
%     X(1,1)=agg_spaces(1);
%     X(1,2)=X(1,1)+agg_lengths(1);
%     for i=2:N_agg
%         X(i,1)=X(i-1,2)+agg_spaces(i);
%         X(i,2)=X(i,1)+agg_lengths(i);
%     end
    
    Q=null(ones(1,N_agg));
    agg_lengths_less_mean=l_agg_std_dev/sqrt(N_agg/(N_agg-1))*Q*randn(N_agg-1,1);
    std_dev_agg_lengths=std(agg_lengths_less_mean);
    agg_lengths=l_agg_av+agg_lengths_less_mean;
    agg_left_ends=zeros(N_agg,1);
    agg_right_ends=ones(N_agg,1);
    next_agg_left_ends=zeros(N_agg,1);
    while (sum(agg_right_ends<next_agg_left_ends)~=N_agg || agg_left_ends(1)<0)
    agg_ctrs=sort(rand(N_agg,1));
    agg_left_ends=agg_ctrs-agg_lengths/2;
    agg_right_ends=agg_ctrs+agg_lengths/2;
    next_agg_left_ends=[agg_left_ends(2:end);L];
    X=[agg_left_ends,agg_right_ends];
    end

end
end

